export default {
  root: "http://localhost:3000",
  login: "http://localhost:3000/login",
  projects: "http://localhost:3000/projects",
  sections: "http://localhost:3000/sections",
  topics: "http://localhost:3000/topics",
  tasks: "http://localhost:3000/tasks",
  skills: "http://localhost:3000/skills",
  signup: "http://localhost:3000/signup",
  me: "http://localhost:3000/me",
  splashform: "http://localhost:3000/splashform",
  invite: "http://localhost:3000/invite",
  users: "http://localhost:3000/users",
};
