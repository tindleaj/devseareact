# Devsea Front End

This repository contains the code and resources for the Devsea Front
End.

Devsea is a crowdsourced web development marketplace. For more
information, see [devsea.io](http://devsea.io/).

## Requirements

In order to hack on Devsea, you will need the following software:

* [yarn](https://yarnpkg.com/en/) (alternatively, [npm](https://npmjs.com/))

These can be installed as follows:

### Arch Linux

```sh
$ sudo pacman -S yarn
```

### Ubuntu (16.04)

```sh
$ sudo apt-get install yarn
```

## Getting Started

First, to use the full-extent of the front end, you will want to
install and configure the Devsea back end. Refer to
the
[Devsea back end documentation](https://github.com/lachose1/devsea/blob/master/README.md) for
instructions.

To build the application, first install dependencies by running:
```sh
$ yarn install
```

Then build the application by running:
```sh
yarn run build
```

To watch the files for changes, run:
```sh
yarn run watch
```

Once webpack is watching for changes, you can spin up the develompmnent server by running:
```sh
yarn run serve
```

Running watch and serve simultaneously is the best way to develop on the Devsea frontend.

## Standards and Conventions

Devsea follows
the [Devsea JS Style Guide](doc/style/js-style-guide.md). Git hooks as
well as an eslint configuration are provided to facilitate the
application and upkeep of these standards. Refer to the separate
`doc/style/js-style-guide.md` file for more details.

## Workflow

The Devsea project uses the
[GitHub Flow](https://guides.github.com/introduction/flow/). The
`master` branch must always be in a production-ready state. If you
want to work on a feature or a fix, just branch off the latest master,
commit your work on that branch, and make a pull request. Your work
will be reviewed and discussed in that pull request. Once all
automated checks have cleared, and the pull request has been
peer-reviewed, it is rebased and merged onto master.

Make sure to rebase your branch on top of master (or the branch you
started off from) regularly to avoid large conflicts at integration
time. Developers are responsible to resolve their own conflicts on
their pull requests before it is eligible to be merged into master.

### Branch Names

Branch names should be descriptive of the feature or fix that they
contain. For Trello integration, you will also want to prefix the
branch name with the card number. For instance, if you are working
on a card #123 which covers the spline reticulating feature, your
branch would be named `123-spline-reticulator`.

To display Trello card numbers in Chrome, you may use
the
[Trello Card Numbers extension](https://chrome.google.com/webstore/detail/trello-card-numbers/kadpkdielickimifpinkknemjdipghafn).

### Commit Messages

Commit messages should be short but descriptive. Use the first line as
a summary, and the body of the commit for more detailed
explanations. Try to keep the first line below 50 characters, and wrap
the body at 72 characters wide.

Use the present tense in the first line of the commit. For instance,
favour "Add frobnicator" over "Added frobnicator".

For more details on our commit message guidelines and the rationale,
refer to
[How to Write a Git Commit Message](http://chris.beams.io/posts/git-commit/)
by Chris Beams.
