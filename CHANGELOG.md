# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased] -
### Added
- User authentication with app state persistence, expiring tokens, and confirm-user/forgot password components
- Variable navigation depending on authentication status and current applications state, a searchbar (placeholder), and user profile dropdown
- Sidebar navigation component with project switch subcomponent
- Task management view (used to be Overview) to show current sections and topics that have active tasks
- Task Kanban to edit tasks and task status

## [0.0.1] - 2017-06-19
### Added
- Changelog.md
- Readme.md
- Initial project boilerplate for react/redux/webpack
- Webpack configuration for handing code bundling and asset optimization
- Initial build of boilerplate in dist/
- Authentication routes, including login, logout, signup, confirm-user, and forgot-password
