/* eslint no-underscore-dangle:0 */

import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import reduxThunk from "redux-thunk";
import ReduxPromise from "redux-promise";
import jwtDecode from "jwt-decode";
import { ToastContainer } from "react-toastify";

import ProjectOverview from "./components/projects/project-overview-container";
import NewProject from "./components/projects/new-project";
import TaskManagement from "./components/task-management/task-management";
import TopicContainer from "./components/kanban/topic-container";
import requireAuth from "./components/auth/require-auth";
import Nav from "./components/nav/nav";
import Sidebar from "./components/sidebar/sidebar";
import Signup from "./components/auth/signup";
import Login from "./components/auth/login";
import Logout from "./components/auth/logout";
import ConfirmUser from "./components/auth/confirm-user";
import ForgotPassword from "./components/auth/forgot-password";
import JoinSplashpage from "./components/join-splashpage/join-splashpage";
import Leaderboard from "./components/leaderboard/leaderboard";
import Welcome from "./components/welcome/welcome-container";
import BrowseProjects from "./components/discover/browse-projects/browse-projects-container";
import DemoLogin from "./components/demo-login";

import reducers from './reducers';
import { AUTH_USER } from './actions/types';
import './main.scss';
import ProfileContainer from './components/profile/profile-container';

const createStoreWithMiddleware = applyMiddleware(reduxThunk, ReduxPromise)(
  createStore
);
const store = createStoreWithMiddleware(
  reducers,
  // This is for the redux dev tools chrome extention
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

// Automatic auth
const token = localStorage.getItem("token");

if (token) {
  const expTimestamp = jwtDecode(token).exp;
  const currentTimestamp = Math.round(new Date().getTime() / 1000);

  if (expTimestamp > currentTimestamp) {
    store.dispatch({
      type: AUTH_USER
    });
  }
}

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <div
        className="columns is-marginless is-mobile"
        style={{ height: "100%" }}
      >
        <ToastContainer
          position="bottom-right"
          autoClose={3000}
          hideProgressBar
        />
        <Route path="/app" component={Sidebar} />
        <div
          className="column is-paddingless"
          style={{ position: "relative", height: "100%" }}
        >
          <Nav />
          <Switch>
            <Route path="/auth/signup/:token" component={Signup} />
            <Route path="/auth/signup" component={Signup} />
            <Route path="/auth/login" component={Login} />
            <Route path="/auth/logout" component={Logout} />
            <Route path="/auth/confirm" component={ConfirmUser} />
            <Route path="/auth/forgot" component={ForgotPassword} />

            <Route path="/join" component={JoinSplashpage} />

            <Route
              path="/discover/browse"
              component={requireAuth(BrowseProjects)}
            />

            <Route path="/demologin" component={DemoLogin} />

            <Route path="/app/welcome" component={requireAuth(Welcome)} />
            <Route
              path="/app/projects/new"
              component={requireAuth(NewProject)}
            />
            <Route
              path="/app/project-overview"
              component={requireAuth(ProjectOverview)}
            />
            <Route path="/app/welcome" component={requireAuth(Welcome)} />
            <Route
              path="/app/task-management"
              component={requireAuth(TaskManagement)}
            />
            <Route
              path="/app/leaderboard"
              component={requireAuth(Leaderboard)}
            />
            <Route
              path="/app/topic/:topicId"
              component={requireAuth(TopicContainer)}
            />
            <Route 
              path="/app/me"
              component={requireAuth(ProfileContainer)} 
            />
            <Redirect from="/app" to="/app/task-management" push />
            <Redirect from="/" to="/join" push />
          </Switch>
        </div>
      </div>
    </BrowserRouter>
  </Provider>,
  document.querySelector("#root")
);
