import axios from "axios";
import moment from "moment";

import { toast } from "react-toastify";

// Routes import can either be an exported object, or json file, should work the same
import routes from "../../config/routes";
import * as types from "./types";

/*------------- Axios Default Config ----------*/
axios.defaults.headers.common["Authorization"] =
  `Bearer ${localStorage.getItem("token")}` || null;

/*------------- Helper Functions --------------*/
function activeProjectFinder(projectsList) {
  const localActiveProject = JSON.parse(localStorage.getItem("activeProject"));

  if (!localActiveProject) return projectsList[0];

  const activeProject = projectsList.find(
    project => project.id === localActiveProject.id
  );

  if (!activeProject) return projectsList[0];

  return activeProject;
}

function updateTopicPoints(topics, tasks) {
  return topics.map(topic => {
    topic.totalPoints = 0;

    tasks.forEach(task => {
      if (topic.id === task.id_project_section_topic)
        topic.totalPoints += task.points || 0;
    });

    return topic;
  });
}

/*------------- Actions --------------*/

// export function fetchInvites() {
//   return {
//     type: FETCH_INVITES,
//     payload: axios.get()
//   }
// }

export function inviteEmailToProject(email, id_project) {
  return function(dispatch) {
    axios({
      method: "post",
      url: routes.invite,
      data: { email, id_project }
    })
      .then(res => {
        dispatch(handleSuccess(res));
      })
      .catch(err => {
        dispatch(handleError(err));
        dispatch({
          type: types.ERRORS.INVITE_EMAIL_TO_PROJECT,
          payload: err.response
        });
      });
  };
}

export function fetchActiveProjectLeaderboard(activeProjectId) {
  return function(dispatch, getState) {
    axios({
      method: "get",
      url: `${routes.projects}/${activeProjectId}/leaderboard`
    }).then(res => {
      let leaderboard = {};
      leaderboard.contributors = res.data
        .map(
          ({
            id,
            username,
            firstname,
            lastname,
            pointsPool,
            pointsAwarded
          }) => {
            return {
              id,
              username,
              firstname,
              lastname,
              pointsPool,
              pointsAwarded,
              tasksCompleted: getState().tasks.tasksList.reduce(
                (accumulator, task) => {
                  if (task.id_user === id && task.finished) {
                    return accumulator + 1;
                  }
                  return accumulator;
                },
                0
              )
            };
          }
        )
        .sort((a, b) => {
          return b.pointsAwarded - a.pointsAwarded;
        });

      leaderboard.totalPoints = leaderboard.contributors.reduce(
        (currentValue, contributor) => {
          return parseInt(contributor.pointsAwarded, 10) + currentValue;
        },
        0
      );

      dispatch({
        type: types.FETCH_ACTIVE_PROJECT_LEADERBOARD,
        payload: leaderboard
      });
    });
  };
}

export function notify(notification) {
  return () => {
    if (typeof notification === "string") {
      return toast(notification, {
        className: "info-toast"
      });
    }

    switch (notification.type) {
      case "success":
        toast(notification.text, {
          className: "success-toast"
        });
        break;

      case "danger":
        toast(notification.text, {
          className: "error-toast"
        });
        break;

      case "error":
        toast(notification.text, {
          className: "error-toast"
        });
        break;

      default:
        toast(notification.text, {
          className: "info-toast"
        });
        break;
    }
  };
}

export function loginUser(username, password, history) {
  // dispatch is from the reduxThunk middleware, gives direct access to the
  // redux dispatch method. (http://redux.js.org/docs/api/Store.html#dispatch)
  return function(dispatch) {
    axios
      .post(`${routes.login}`, { username, password })
      .then(res => {
        localStorage.setItem("token", res.data.token);
        // Set the axios default header to the token on login, fixes the initial page load bug
        axios.defaults.headers.common["Authorization"] =
          `Bearer ${localStorage.getItem("token")}` || null;

        dispatch(authUser());
        history.push("/app/welcome");
      })
      .catch(err => {
        dispatch(handleError(err));
        dispatch({
          type: types.ERRORS.LOGIN_USER,
          payload: err.response
        });
      });
  };
}

export function hideSidebar() {
  return {
    type: types.HIDE_SIDEBAR
  };
}

export function showSidebar() {
  return {
    type: types.SHOW_SIDEBAR
  };
}

export function authUser() {
  return {
    type: types.AUTH_USER
  };
}

// General error handling action, might need more robust version in the future
export function handleError(err, message = "") {
  return function(dispatch) {
    console.error(err);
    if (err.response.data) {
      dispatch(
        notify({
          type: "error",
          text: `${Object.keys(err.response.data)[0].toUpperCase()}: ${
            Object.values(err.response.data)[0]
          }`
        })
      );
    } else {
      dispatch(
        notify({
          type: "error",
          text: err
        })
      );
    }
  };
}

// This should probably be handled differently, some kind
// of app-wide notification system
export function handleSuccess(res, message = "") {
  return function(dispatch) {
    console.log(res);
    if (res.data.success) {
      dispatch(
        notify({
          type: "success",
          text: `${Object.keys(res.data)[0].toUpperCase()}: ${
            Object.values(res.data)[0]
          }`
        })
      );
    }
  };
}

export function confirmUser(email) {
  return {
    type: types.CONFIRM_USER,
    payload: email
  };
}

export function logoutUser() {
  localStorage.removeItem("token");
  localStorage.removeItem("activeProject");
  return {
    type: types.UNAUTH_USER
  };
}

export function fetchAll() {
  return (dispatch, getState) => {
    if (getState().projects.activeProject) return;

    axios.get(`${routes.projects}`).then(res => {
      const projectsList = res.data;

      if (projectsList.length === 0)
        return dispatch({ type: types.FETCH_PROJECTS, payload: projectsList });

      const activeProject = activeProjectFinder(projectsList);

      dispatch(setActiveProject(activeProject));
      dispatch({ type: types.FETCH_PROJECTS, payload: projectsList });
      dispatch(fetchProjectData(activeProject.id));
      dispatch(fetchActiveProjectLeaderboard(activeProject.id));
    });
  };
}

export function fetchProjectData(projectId) {
  const requests = [
    axios.get(`${routes.projects}/${projectId}/sections`),
    axios.get(`${routes.projects}/${projectId}/topics`),
    axios.get(`${routes.projects}/${projectId}/tasks`),
    axios.get(`${routes.projects}/${projectId}/users`),
    axios.get(`${routes.me}`)
  ];

  return dispatch => {
    axios.all(requests).then(
      axios.spread((sections, topics, tasks, users, me) => {
        topics.data = updateTopicPoints(topics.data, tasks.data);

        dispatch({ type: types.FETCH_SECTIONS, payload: sections });
        dispatch({ type: types.FETCH_TOPICS, payload: topics });
        dispatch({ type: types.FETCH_TASKS, payload: tasks });
        dispatch({ type: types.FETCH_USERS, payload: users });
        dispatch({ type: types.FETCH_ME, payload: me });
      })
    );
  };
}

export function updateMe(me) {
  return dispatch => {
    return axios.put(`${routes.me}`, me).then(res => {
      dispatch(notify({ type: "success", text: "Profile saved." }));
      dispatch({
        type: types.UPDATE_ME,
        payload: { data: me }
      });
    });
  };
}

export function updateProfilePicture(userId, picture) {
  return dispatch => {
    return axios
      .post(`${routes.users}/${userId}/profile-picture`, picture)
      .then(res => {
        dispatch(notify({ type: "success", text: "Picture updated." }));
        dispatch({
          type: types.UPDATE_PROFILE_PICTURE,
          payload: { data: res.data }
        });
      });
  };
}

export function setActiveProject(project) {
  localStorage.setItem("activeProject", JSON.stringify(project));

  return dispatch => {
    dispatch(fetchProjectData(project.id));
    dispatch({ type: types.SET_ACTIVE_PROJECT, payload: project });
  };
}

export function updateProject(project) {
  return {
    type: types.UPDATE_PROJECT,
    payload: axios.put(`${routes.projects}/${project.id}`, project)
  };
}

export function addProject(project) {
  return {
    type: types.ADD_PROJECT,
    payload: axios.post(routes.projects, project)
  };
}

export function addSection(section) {
  return (dispatch, getState) => {
    const max_num_row = Math.max(
      ...getState().sections.sectionsList.map(section =>
        parseFloat(section.num_row)
      )
    );

    section.num_row = max_num_row >= 0 ? max_num_row + 5000 : 5000;

    return axios.post(`${routes.sections}`, section).then(res => {
      dispatch({
        type: types.ADD_SECTION,
        payload: res
      });
      return res.data;
    });
  };
}

export function deleteSection(section) {
  return dispatch => {
    axios.delete(`${routes.sections}/${section.id}`).then(res => {
      dispatch({
        type: types.DELETE_SECTION,
        payload: { data: section }
      });
    });
  };
}

export function updateSection(section) {
  return {
    type: types.UPDATE_SECTION,
    payload: axios.put(`${routes.sections}/${section.id}`, section)
  };
}

export function addTopic(topic) {
  return {
    type: types.ADD_TOPIC,
    payload: axios.post(`${routes.topics}`, topic)
  };
}

export function updateTopic(topic) {
  return {
    type: types.UPDATE_TOPIC,
    payload: axios.put(`${routes.topics}/${topic.id}`, topic)
  };
}

export function deleteTopic(topic) {
  return dispatch => {
    axios.delete(`${routes.topics}/${topic.id}`).then(res => {
      dispatch({
        type: types.DELETE_TOPIC,
        payload: { data: topic }
      });
    });
  };
}

export function setActiveTopic(topicId) {
  return {
    type: types.SET_ACTIVE_TOPIC,
    payload: topicId
  };
}

export function setActiveTask(task) {
  return {
    type: types.SET_ACTIVE_TASK,
    payload: task
  };
}

export function addTask(task) {
  return (dispatch, getState) => {
    const max_num_row = Math.max(
      ...getState().tasks.tasksList.map(task => parseFloat(task.num_row))
    );

    task.num_row = max_num_row >= 0 ? max_num_row + 5000 : 5000;

    axios.post(`${routes.tasks}`, task).then(res => {
      dispatch({
        type: types.ADD_TASK,
        payload: res,
        points: res.data.points
      });
    });
  };
}

export function updateTask(newTask) {
  return (dispatch, getState) => {
    const currentActiveTask = getState().tasks.activeTask;
    const oldTask = getState().tasks.tasksList.find(
      task => task.id === newTask.id
    );

    if (newTask.finished && !newTask.id_project) newTask.num_col = 3;
    if (
      (!newTask.finished || newTask.finished === "false") &&
      newTask.num_col === 3
    )
      newTask.num_col = 1;

    axios.put(`${routes.tasks}/${newTask.id}`, newTask).then(res => {
      if (typeof res.data.finished === "string")
        res.data.finished = res.data.finished === "true";

      dispatch({
        type: types.UPDATE_TASK,
        payload: res,
        points: res.data.points - oldTask.points,
        activeTask: currentActiveTask ? res.data : currentActiveTask
      });
    });
  };
}

export function deleteTask(task) {
  return dispatch => {
    axios.delete(`${routes.tasks}/${task.id}`).then(res => {
      dispatch({
        type: types.DELETE_TASK,
        payload: { data: task },
        points: -task.points
      });
    });
  };
}

export function finishTask(task) {
  return dispatch => {
    const finishedTask = task;

    finishedTask.finished = "true";
    finishedTask.date_finished = moment().format("YYYY-MM-DD");
    finishedTask.num_col = 2;

    dispatch(updateTask(finishedTask));
  };
}

export function unfinishTask(task) {
  return dispatch => {
    const unfinishedTask = task;

    unfinishedTask.finished = "false";
    unfinishedTask.date_finished = null;
    unfinishedTask.num_col = 1;

    dispatch(updateTask(unfinishedTask));
  };
}

export function addTaskUser(taskId, taskUser) {
  return dispatch => {
    axios.post(`${routes.tasks}/${taskId}/users`, taskUser).then(res => {
      dispatch({ type: types.ADD_TASK_USER, payload: res, taskId });
    });
  };
}

export function addWaitUser(wait_list_user) {
  return {
    type: types.ADD_WAIT_USER,
    payload: axios.post(`${routes.splashform}`, wait_list_user)
  };
}

export function signupUser(
  firstname,
  lastname,
  username,
  email,
  password,
  token = null,
  history
) {
  return function(dispatch) {
    axios
      .post(`${routes.signup}`, {
        firstname,
        lastname,
        username,
        email,
        password,
        token
      })
      .then(res => {
        dispatch(confirmUser(res.data.email));

        history.push("/auth/confirm");
      })
      .catch(err => {
        dispatch(handleError(err));
        dispatch({
          type: types.ERRORS.SIGNUP_USER,
          payload: err.response
        });
      });
  };
}

export function fetchSkills() {
  return dispatch => {
    axios.get(`${routes.skills}`).then(payload => {
      dispatch({ type: types.FETCH_SKILLS, payload });
    });
  };
}
