export const AUTH_USER = "auth_user";
export const UNAUTH_USER = "unauth_user";
export const AUTH_ERROR = "auth_error";
export const CONFIRM_USER = "confirm_user";
export const FETCH_PROJECTS = "fetch_projects";
export const FETCH_PROJECT_TASKS = "fetch_project_tasks";
export const FETCH_SECTIONS = "fetch_sections";
export const FETCH_ME = "fetch_me";
export const UPDATE_ME = "update_me";
export const SET_ACTIVE_PROJECT = "set_active_project";
export const ADD_PROJECT = "add_project";
export const UPDATE_PROJECT = "update_project";
export const ADD_TOPIC = "add_topic";
export const UPDATE_TOPIC = "update_topic";
export const DELETE_TOPIC = "delete_topic";
export const FETCH_TOPICS = "fetch_topics";
export const SET_ACTIVE_TOPIC = "set_active_topic";
export const FETCH_TASKS = "fetch_tasks";
export const SET_ACTIVE_TASK = "set_active_task";
export const UPDATE_TASK = "update_task";
export const ADD_TASK = "add_task";
export const DELETE_TASK = "delete_task";
export const ADD_WAIT_USER = "add_wait_user";
export const FETCH_USERS = "fetch_users";
export const SHOW_SIDEBAR = "show_sidebar";
export const HIDE_SIDEBAR = "hide_sidebar";
export const UPDATE_SECTION = "update_section";
export const ADD_SECTION = "add_section";
export const DELETE_SECTION = "delete_section";
export const ADD_TASK_USER = "add_task_user";
export const UPDATE_PROFILE_PICTURE = "update_profile_picture";
export const FETCH_ACTIVE_PROJECT_LEADERBOARD =
  "fetch_active_project_leaderboard";
export const FETCH_SKILLS = "fetch_skills";
export const FETCH_INVITES = "fetch_invites";
export const ERRORS = {
  SIGNUP_USER: "error_signup_user",
  LOGIN_USER: "error_login_user",
  INVITE_EMAIL_TO_PROJECT: "error_invite_email_to_project"
};
