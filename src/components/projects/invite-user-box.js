import React from "react";
import { Field, reduxForm } from "redux-form";

const handleFormSubmit = (values, props) => {
  props.inviteAction(values.email, props.id);
};

const InviteUserBox = props => {
  const { handleSubmit, pristine, reset, submitting } = props;
  return (
    <div className="box">
      <h4 className="title is-5">Invite new user</h4>
      <form onSubmit={handleSubmit(values => handleFormSubmit(values, props))}>
        <div className="field has-addons">
          <div className="control is-expanded has-icons-left">
            <Field
              component="input"
              name="email"
              className="input"
              type="text"
              placeholder="user@email.com"
            />
            <span className="icon is-small is-left">
              <i className="fa fa-envelope" />
            </span>
          </div>
          <div className="control">
            <button type="submit" className="button is-primary">
              Invite
            </button>
          </div>
        </div>
      </form>
    </div>
  );
};

export default reduxForm({
  form: "invtiteEmailToProject"
})(InviteUserBox);
