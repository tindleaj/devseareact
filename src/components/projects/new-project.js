import React, { Component } from 'react';

import NewProjectForm from './new-project-form';

class NewProject extends Component {
	render() {
		return (
			<section className="section ds-content has-background-white">
				<h2 className="title is-2 has-text-centered">
					Create New Project
				</h2>
				<NewProjectForm />
			</section>
		);
	}
}

export default NewProject;
