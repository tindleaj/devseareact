import React, { Component } from "react";

class ProjectOverviewDetails extends Component {
  constructor(props) {
    super(props);

    this.state = {
      changed: false,
      name: props.activeProject.name,
      bio: props.activeProject.bio || ""
    };
  }

  updateProjectField(field, value) {
    const newState = this.state;

    newState.changed = true;
    newState[field] = value;

    this.setState(newState);
  }

  handleUpdateProject(e) {
    e.preventDefault();

    this.setState({ changed: false });

    this.props.updateProject({ ...this.props.activeProject, ...this.state });
  }

  render() {
    if (this.refs.bio) {
      this.refs.bio.style.height = "1px";
      this.refs.bio.style.height = `${this.refs.bio.scrollHeight}px`;
    }

    return (
      <section className="hero">
        <div className="hero-body">
          <div className="columns">
            <figure
              className="ds-background-image ds-project-overview-image column"
              style={{
                backgroundImage: `url(https://picsum.photos/600/400?image=${
                  this.props.activeProject.id
                })`
              }}
            />
            <div className="column">
              <form onSubmit={this.handleUpdateProject.bind(this)}>
                <div className="field ds-project-overview-field">
                  <div className="control">
                    <input
                      className="input is-size-3"
                      disabled={false}
                      value={this.state.name}
                      onChange={e =>
                        this.updateProjectField("name", e.target.value)
                      }
                    />
                  </div>
                </div>
              </form>

              <article className="ds-project-overview-progress">
                <progress
                  className="progress is-primary"
                  value={this.props.tasks.completed}
                  max={this.props.tasks.total}
                >
                  {(
                    (this.props.tasks.completed / this.props.tasks.total) *
                    100
                  ).toFixed()}%
                </progress>
                <span className="ds-progress-text">
                  Tasks completed: {this.props.tasks.completed}/{
                    this.props.tasks.total
                  }
                </span>
              </article>
            </div>
          </div>
          <div>
            <h5 className="title is-5 has-text-weight-bold">
              About this project
            </h5>
            <form onSubmit={this.handleUpdateProject.bind(this)}>
              <div className="field ds-project-overview-field">
                <div className="control">
                  <textarea
                    className="textarea"
                    ref="bio"
                    disabled={false}
                    value={this.state.bio}
                    onChange={e =>
                      this.updateProjectField("bio", e.target.value)
                    }
                  />
                </div>
              </div>
              <div className="field">
                <div className="control">
                  {this.state.changed && this.state.name && this.state.bio ? (
                    <button className="button is-info" type="submit">
                      Save
                    </button>
                  ) : null}
                </div>
                <p className="help has-text-danger">
                  {!this.state.name || !this.state.bio
                    ? "Project name or bio cannot be empty"
                    : ""}
                </p>
              </div>
            </form>
          </div>
        </div>
      </section>
    );
  }
}

export default ProjectOverviewDetails;
