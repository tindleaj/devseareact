import React from 'react';
import { reduxForm, Field } from 'redux-form';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import * as actions from '../../actions';

const validate = values => {
	const errors = {};
	if (!values.name) {
		errors.name = 'Project name is required';
	}

	return errors;
};

const handleFormSubmit = (props, project) => {
	props.addProject(project);
	props.notify({ type: 'success', text: 'Project created!' });
	props.history.go(-1);
};

const createRenderer = render => ({
	input,
	meta,
	label,
	placeholder,
	...rest
}) => (
	<div className="field">
		<label className="label">{label}</label>
		<div className="control is-expanded">
			{render(input, placeholder, meta, rest)}
		</div>
		{meta.error &&
			meta.touched && <p className="help is-danger">{meta.error}</p>}
	</div>
);

// Renderers for different form components
const RenderInput = createRenderer((input, placeholder, meta) => (
	<input
		{...input}
		placeholder={placeholder}
		className={[
			meta.error && meta.touched ? 'is-danger' : '',
			'input'
		].join(' ')}
	/>
));

const RenderTextArea = createRenderer((input, label, meta) => (
	<textarea
		{...input}
		placeholder={label}
		className={[
			meta.error && meta.touched ? 'is-danger' : '',
			'textarea'
		].join(' ')}
	/>
));

let ProjectAddForm = props => {
	const { handleSubmit, submitting } = props;
	return (
		<form
			className="ds-new-project-form"
			onSubmit={handleSubmit(project => handleFormSubmit(props, project))}
		>
			<Field name="name" label="Project name" component={RenderInput} />
			<Field
				name="description"
				label="Description"
				component={RenderTextArea}
			/>

			<div className="field">
				<button
					action="submit"
					className="button ds-primary-button is-secondary-blue"
					disabled={submitting}
				>
					Create New Project
				</button>
			</div>
		</form>
	);
};

ProjectAddForm = reduxForm({
	form: 'addProject',
	validate
})(ProjectAddForm);

export default withRouter(connect(null, actions)(ProjectAddForm));
