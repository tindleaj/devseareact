import React from "react";
import TeamListMember from "./team-list-member";

const TeamListBox = props => {
  return (
    <div className="box">
      <h4 className="title is-4">{props.activeProject.name} Team</h4>
      {props.usersList.map(user => {
        console.log(user);
        return (
          <TeamListMember
            image={user.picture_url}
            name={user.firstname + user.lastname}
            username={user.username}
            id={user.id}
            email={user.email}
            status={user.date_registered ? "active" : "inactive"}
            activeSince={user.date_registered}
            key={user.id}
          />
        );
      })}
    </div>
  );
};

export default TeamListBox;
