import React, { Component } from "react";
import { connect } from "react-redux";
import * as actions from "../../actions/index";

import ProjectOverviewDetails from "./project-overview-details";
import InviteUserBox from "./invite-user-box";
import TeamListBox from "./team-list-box";

class ProjectOverviewContainer extends Component {
  computeTasks(tasks) {
    let total = tasks.length;
    let completed = tasks.filter(task => {
      return task.finished;
    }).length;

    return {
      total,
      completed
    };
  }

  render() {
    return (
      <section className="section ds-content">
        <h4 className="title is-4 ds-component-title">Project Overview</h4>
        <div className="ds-project-overview">
          <ProjectOverviewDetails
            activeProject={this.props.activeProject}
            updateProject={this.props.updateProject}
            tasks={this.computeTasks(this.props.tasksList)}
          />
          <div className="columns">
            <div className="column">
              <TeamListBox
                activeProject={this.props.activeProject}
                usersList={this.props.usersList}
                invitesList={this.props.invitesList}
              />
              <InviteUserBox
                id={this.props.activeProject.id}
                inviteAction={this.props.inviteEmailToProject}
              />
            </div>
            <div className="column" />
          </div>
        </div>
      </section>
    );
  }
}

function mapStateToProps(state) {
  return {
    activeProject: state.projects.activeProject,
    tasksList: state.tasks.tasksList,
    usersList: state.users.usersList,
    invitesList: {}
  };
}

export default connect(
  mapStateToProps,
  actions
)(ProjectOverviewContainer);
