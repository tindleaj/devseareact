import React from "react";

const TeamListMember = props => {
  return (
    <div className="">
      <div className="media">
        <div className="media-left">
          <figure className="image is-64x64">
            <img className="" src={props.image} alt="Profile" />
          </figure>
        </div>
        <div className="media-content">
          <div className="level is-marginless">
            <div className="level-left">
              <h5 className="is-size-6 has-text-weight-bold">{props.name}</h5>
              &nbsp; - &nbsp;
              <h6 className="has-text-grey is-size-7">@{props.username}</h6>
            </div>
            <span
              className={`${
                props.status === "active" ? "is-success" : "is-info"
              } tag is-size-8 level-right is-capitalized`}
            >
              {props.status}
            </span>
          </div>
          <div className="">
            <p className="is-size-7">
              Active since:&nbsp;
              <span className="has-text-grey">{props.activeSince}</span>
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default TeamListMember;
