import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import Authed from './authed';
import Unauthed from './unauthed';

class Nav extends Component {
  renderNav() {
    if (this.props.authenticated && this.props.location.pathname !== "/join") {
      return <Authed />;
    } 

    if (this.props.location.pathname !== "/join") {
      return <Unauthed />;
    }
  }

  render() {
    return (
      <div>
        {this.renderNav()}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    authenticated: state.auth.authenticated
  };
}

export default withRouter(connect(mapStateToProps)(Nav));
