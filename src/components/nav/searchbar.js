import React from 'react';

const SearchBar = () =>
  <div className="level-left">
    <div className="level-item">
      <div className="field has-addons ds-searchbar">
        <div className="control has-icons-left">
          <input className="input" type="text" placeholder="Search" />
          <span className="icon is-small is-left">
            <i className="fa fa-search" />
          </span>
        </div>
        <div className="control">
          <div className="select">
            <select>
              <option>All</option>
            </select>
          </div>
        </div>
      </div>
    </div>
  </div>;

export default SearchBar;
