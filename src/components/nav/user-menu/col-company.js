import React from 'react';

const CompanyColumn = () =>
  <div className="column">
    <div className="dropdown-item">
      <h3 className="is-uppercase">
        <strong>Company</strong>
      </h3>
    </div>
    <div className="dropdown-item">
      <a className="button is-info is-outlined is-uppercase is-size-9">
        <strong>Add New Company</strong>
      </a>
    </div>
  </div>;

export default CompanyColumn;
