import React, { Component } from 'react';
import cn from 'classnames';

import UserMenuDropdown from './user-menu-dropdown';

class UserMenuButton extends Component {
  constructor(props) {
    super(props);

    this.state = {
      dropdownIsActive: false
    };

    this.dropdownCollapse = this.dropdownCollapse.bind(this);
  }

  // Set the state of the dropdown, update css classes accordingly
  dropdownToggle() {
    this.setState({ dropdownIsActive: !this.state.dropdownIsActive });
  }

  dropdownCollapse() {
    this.setState({ dropdownIsActive: false });
  }

  renderBackground(dropdownIsActive) {
    if (!dropdownIsActive) return null;

    return (
      <div
        className="ds-menu-dropdown-background"
        onClick={() => {
          this.setState({ dropdownIsActive: false });
        }}
      />
    );
  }

  render() {
    const cnClass = cn('level-item dropdown is-right', {
      'is-active': this.state.dropdownIsActive
    });

    return (
      <div className={cnClass}>
        <a
          tabIndex="0"
          className="level-item has-text-centered ds-user-menu-button"
          onClick={() => this.dropdownToggle()}
          // this breaks the menu links as it is
          // onBlur={() => this.dropdownCollapse()}
        >
          <img
            src="https://i.pinimg.com/236x/8d/a1/39/8da1395cce1ee57ebab406b72af11031--mature-old-old-person.jpg"
            alt=""
            style={{ height: '40px', borderRadius: '50%' }}
          />
        </a>
        {this.renderBackground(this.state.dropdownIsActive)}
        <UserMenuDropdown dropdownCollapse={this.dropdownCollapse} />
      </div>
    );
  }
}

export default UserMenuButton;
