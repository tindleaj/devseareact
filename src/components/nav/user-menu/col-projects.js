import React from 'react';
import { Link, withRouter } from 'react-router-dom';

const ProjectsColumn = props => {
  function renderProjects(projectsList) {
    if (!projectsList) return null;

    return projectsList.map(project => (
      <a
        onClick={() => {
          props.setActiveProject(project);
          props.history.push('/app/task-management');
          props.dropdownCollapse();
        }}
        className="project-item"
        key={project.id}
      >
        <figure className="image is-48x48 is-round">
          <img
            src="https://bulma.io/images/placeholders/48x48.png"
            alt="Project"
          />
        </figure>
        <span>{project.name}</span>
      </a>
    ));
  }

  return (
    <div className="column">
      <div className="dropdown-item">
        <h3 className="is-uppercase">
          <strong>Projects</strong>
        </h3>
      </div>
      <div className="ds-projects-container">
        {renderProjects(props.projectsList)}
      </div>
      <div className="dropdown-item">
        <Link
          to="/app/projects/new"
          className="button is-info is-outlined is-uppercase is-size-9"
          onClick={props.dropdownCollapse}
        >
          <strong>Create Project</strong>
        </Link>
      </div>
    </div>
  );
};

export default withRouter(ProjectsColumn);
