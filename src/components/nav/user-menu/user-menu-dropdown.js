import React from 'react';
import { connect } from 'react-redux';

import * as actions from '../../../actions';

import ProfileColumn from './col-profile';
import ProjectsColumn from './col-projects';

const UserMenuDropdown = props => (
	<div className="dropdown-menu ds-user-menu-dropdown" role="menu">
		<div className="dropdown-content">
			<div className="columns">
				<ProjectsColumn
					projectsList={props.projectsList}
					setActiveProject={props.setActiveProject}
					dropdownCollapse={props.dropdownCollapse}
				/>
				<ProfileColumn dropdownCollapse={props.dropdownCollapse} />
			</div>
		</div>
	</div>
);

function mapStateToProps(state) {
	return {
		projectsList: state.projects.projectsList,
	};
}

export default connect(mapStateToProps, actions)(UserMenuDropdown);
