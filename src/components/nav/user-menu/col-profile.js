import React from 'react';
import { Link } from 'react-router-dom';

const ProfileColumn = props => (
  <div className="column">
    <div className="dropdown-item">
      <h3 className="is-uppercase">
        <strong>Profile</strong>
      </h3>
    </div>

    <div className="dropdown-item">
      <Link to="/app/me" onClick={props.dropdownCollapse}>
        My profile
      </Link>
    </div>

    <div className="dropdown-item">
      <Link to="">Preferences</Link>
    </div>

    <div className="dropdown-item">
      <Link to="">Help</Link>
    </div>

    <div className="dropdown-item">
      <Link to="">Report an issue</Link>
    </div>

    <br />

    <div className="dropdown-item">
      <Link to="/auth/logout">Logout</Link>
    </div>
  </div>
);

export default ProfileColumn;
