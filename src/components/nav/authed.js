import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as actions from '../../actions';

import SearchBar from './searchbar';
import UserMenuButton from './user-menu/user-menu-button';

class Authed extends Component {
  render() {
    return (
      <nav className="ds-navbar level is-mobile">
        <SearchBar />
        <div className="level-right">
          <div className="level-item nav-icon">
            <a className="icon">
              <i className="fa fa-comments" />
            </a>
          </div>
          <div className="level-item nav-icon">
            <a className="icon">
              <i className="fa fa-bell" />
            </a>
          </div>
          <UserMenuButton />
        </div>
      </nav>
    );
  }
}

function mapStateToProps(state) {
  return {
    me: state.users.me,
    auth: state.auth
  };
}

export default connect(mapStateToProps, actions)(Authed);
