import React from 'react';
import { Link } from 'react-router-dom';

import logoIcon from '../../img/logo-icon.png';

const UnAuthed = () => (
	<nav className="level ds-navbar ds-navbar-unauthed">
		<div className="level-left">
			<img src={logoIcon} alt="Devsea logo" />
			<h2 className="is-uppercase is-size-3 has-text-white level-item">devsea</h2>
		</div>
		<div className="level-right">
			<Link to="/auth/signup" className="level-item has-text-white">
				<strong>Join</strong>
			</Link>
			<p className="has-text-white">
				Have an account?{' '}
				<Link to="/auth/login" className="has-text-white">
					<strong>Log in</strong>
				</Link>
			</p>
		</div>
	</nav>
);

export default UnAuthed;
