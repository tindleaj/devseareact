import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as actions from '../../actions';

import ProjectTile from './project-tile';

import Background from '../../img/welcome-background.jpg';

class Welcome extends Component {
	componentWillMount() {
		this.props.hideSidebar();
	}

	componentWillUnmount() {
		this.props.showSidebar();
	}

	renderProjectTiles() {
		return this.props.projectsList.map(project => {
			return (
				<ProjectTile
					project={project}
					key={project.id}
					setActiveProject={this.props.setActiveProject}
				/>
			);
		});
	}

	render() {
		return (
			<div
				className="ds-content"
				style={{
					backgroundImage: `url(${Background})`,
					backgroundSize: 'cover',
				}}
			>
				<h2 className="title">{`Welcome back, ${this.props.me.firstname}.`}</h2>
				<br />
				<div className="columns is-flex-wrap">{this.renderProjectTiles()}</div>
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		me: state.users.me,
		projectsList: state.projects.projectsList,
	};
}

export default connect(mapStateToProps, actions)(Welcome);
