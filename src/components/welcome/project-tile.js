import React from 'react';
import { Link } from 'react-router-dom';

export default function ProjectTile(props) {
  return (
    <div className="column is-one-third">
      <div className="card project-card">
        <div className="card-image project-card__image-container">
          <figure className="project-card__image">
            <img
              src={`https://picsum.photos/600/400?image=${props.project.id}`}
              alt="Project"
            />
          </figure>
        </div>
        <div className="card-content">
          <div className="content project-card__content">
            <h3 className="title">{props.project.name}</h3>
            <h4 className="subtitle-ds is-5">{`by ${props.project.name}`}</h4>
            <p className="project-card__description">{props.project.bio}</p>
          </div>
        </div>
        <div className="project-card__footer">
          <Link
            className="button is-info is-fullwidth is-uppercase"
            to="/app/task-management"
            onClick={() => props.setActiveProject(props.project)}
          >
            Go to Project
          </Link>
        </div>
      </div>
    </div>
  );
}
