import React, { Component } from 'react';

import TopicAddModalForm from './topic-add-modal-form';

class TopicAddModal extends Component {
  render() {
    return (
      <div className="modal is-active">
        <div className="modal-background" onClick={() => this.props.onClick()} />
        <div className="modal-card">
          <header className="modal-card-head level is-marginless section">
            <p className="level-left modal-card-title is-uppercase">
              {!this.props.topic ? 'Add topic' : 'Edit topic'}
            </p>
            <span className="level-right" />
          </header>
          <section className="modal-card-body section">
            <TopicAddModalForm onClick={() => this.props.onClick()} topic={this.props.topic} />
          </section>
        </div>
      </div>
    );
  }
}

export default TopicAddModal;
