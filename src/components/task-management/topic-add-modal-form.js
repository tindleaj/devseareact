import React from "react";
import { reduxForm, Field } from "redux-form";
import { connect } from "react-redux";
import * as actions from "../../actions";

const validate = values => {
  const errors = {};
  if (!values.name) {
    errors.name = "Topic name is required";
  }
  if (!values.id_project_section) {
    errors.id_project_section = "Section is required";
  }

  return errors;
};

const handleFormSubmit = (props, topic) => {
  topic = { id_user: props.users.me.id, ...topic };
  if (topic.id_project_section === "new") {
    props
      .addSection({
        id_project: props.activeProject.id,
        name: "New Section"
      })
      .then(newSection => {
        doSubmit(props, { ...topic, id_project_section: newSection.id });
      });
  } else {
    doSubmit(props, topic);
  }
};

const doSubmit = (props, topic) => {
  if (props.topic) {
    props.updateTopic({ id: props.topic.id, ...topic });
    props.notify({ type: "success", text: "Topic updated!" });
  } else {
    props.addTopic(topic);
    props.notify({ type: "success", text: "Topic added!" });
  }

  props.onClick();
};

const createRenderer = render => ({
  input,
  meta,
  label,
  placeholder,
  ...rest
}) => (
  <div className="field is-horizontal">
    <div className="field-label is-normal">
      <label className="label">{label}</label>
    </div>
    <div className="field-body">
      <div className="field">
        <div className="control is-expanded">
          {render(input, placeholder, meta, rest)}
        </div>
        {meta.error &&
          meta.touched && <p className="help is-danger">{meta.error}</p>}
      </div>
    </div>
  </div>
);

// Renderers for different form components
const RenderInput = createRenderer((input, placeholder, meta) => (
  <input
    {...input}
    placeholder={placeholder}
    className={[meta.error && meta.touched ? "is-danger" : "", "input"].join(
      " "
    )}
  />
));

const RenderSelect = createRenderer((input, label, meta, { children }) => (
  <div
    className={[
      meta.error && meta.touched ? "is-danger" : "",
      "select is-fullwidth"
    ].join(" ")}
  >
    <select {...input} className="select">
      {children}
    </select>
  </div>
));

const RenderTextArea = createRenderer((input, label, meta) => (
  <textarea
    {...input}
    placeholder={label}
    className={[meta.error && meta.touched ? "is-danger" : "", "textarea"].join(
      " "
    )}
  />
));

let TopicAddModalForm = props => {
  const { handleSubmit, submitting, topic } = props;
  return (
    <form onSubmit={handleSubmit(topic => handleFormSubmit(props, topic))}>
      <Field name="name" label="Topic name" component={RenderInput} />
      <Field name="id_project_section" label="Section" component={RenderSelect}>
        <option value={null} />
        {props.sectionsList.map(option => {
          return (
            <option key={option.id} value={option.id}>
              {option.name}
            </option>
          );
        })}
        <option value={"new"}>New Section</option>
      </Field>
      <Field
        name="description"
        label="Description"
        component={RenderTextArea}
      />
      <Field name="id_topic_status" label="Status" component={RenderSelect}>
        <option />
        <option value="1">Open</option>
        <option value="2">High-priority</option>
        <option value="3">N/A</option>
        <option value="4">Closed</option>
      </Field>
      <Field name="url_slack" label="Slack link" component={RenderInput} />
      <Field name="url_github" label="Github link" component={RenderInput} />

      <div className="field is-horizontal">
        <div className="field-label is-normal" />
        <div className="field-body">
          <div className="field">
            <button
              action="submit"
              className="button ds-primary-button is-secondary-blue"
              disabled={submitting}
            >
              {!topic ? "Add New Topic" : "Update Topic"}
            </button>
          </div>
        </div>
      </div>
    </form>
  );
};

TopicAddModalForm = reduxForm({
  form: "addTopic",
  validate
})(TopicAddModalForm);

TopicAddModalForm = connect(
  (state, ownProps) => ({
    users: state.users,
    activeProject: state.projects.activeProject,
    sectionsList: state.sections.sectionsList,
    initialValues: {
      name: ownProps.topic ? ownProps.topic.name : null,
      description: ownProps.topic ? ownProps.topic.description : null,
      id_project_section: ownProps.topic ? ownProps.id_project_section : null,
      id_topic_status: ownProps.topic ? ownProps.topic.id_topic_status : "1",
      urlGithub: ownProps.topic ? ownProps.topic.url_github : null,
      urlSlack: ownProps.topic ? ownProps.topic.url_slack : null
    }
  }),
  actions
)(TopicAddModalForm);

export default TopicAddModalForm;
