import React from 'react';
import { Link } from 'react-router-dom';

const TopicDeleteModal = ({ onDismiss, onDelete, topicTasks, topic }) => {
  const count = topicTasks.length;
  const isThereTaskInProgress = count && topicTasks.some(t => +t.num_col !== 0);

  const deleteText = count
    ? isThereTaskInProgress
      ? 'This topic cannot be deleted because one or more of its tasks are in columns other than the To-Do column.'
      : `There are ${
          topicTasks.length
        } task(s) in this topic's To-Do column. Are you sure you want to delete this topic?`
    : 'Are you sure you want to delete this topic?';

  const renderDeleteButton = () =>
    !isThereTaskInProgress && (
      <button
        action="submit"
        className="button ds-primary-button is-red"
        onClick={onDelete}
      >
        Delete Topic
      </button>
    );

  const renderSeeTasksButton = () =>
    count ? (
      <Link to={`/app/topic/${topic.id}`}>
        <button
          action="submit"
          className="button ds-primary-button is-secondary-blue"
        >
          See Tasks
        </button>
      </Link>
    ) : null;
  return (
    <div className="modal is-active">
      <div className="modal-background" onClick={() => onDismiss()} />
      <div className="modal-card">
        <header className="modal-card-head level is-marginless section">
          <p className="level-left modal-card-title is-uppercase">
            Delete Topic {topic.name}
          </p>
          <span className="level-right" />
        </header>
        <section className="modal-card-body section">
          <div className="modal-body">
            <p className="has-text-left content">{deleteText}</p>
          </div>
        </section>
        <section className="modal-card-body section">
          <div className="field">{renderDeleteButton()}</div>
          <div className="field">{renderSeeTasksButton()}</div>
        </section>
      </div>
    </div>
  );
};

export default TopicDeleteModal;
