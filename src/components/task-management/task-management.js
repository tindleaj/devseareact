import React, { Component } from "react";
import { connect } from "react-redux";

import * as actions from "../../actions";

import TopicTile from "./topic-tile";
import TopicAddModal from "./topic-add-modal";
import Loader from "../loader";
import TopicDeleteModal from "./topic-delete-modal";

class TaskManagement extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isAddTopic: false,
      isDeleteTopic: null,
      topicTasksCount: null,
      editTopic: null,
      newSectionName: "",
      isAddFocus: false
    };

    this.editTopic = this.editTopic.bind(this);
    this.deleteTopic = this.deleteTopic.bind(this);
  }

  editTopic(topic) {
    this.setState({ editTopic: topic, isAddTopic: true });
  }

  deleteTopic(topic) {
    this.setState({ isDeleteTopic: true, editTopic: topic });
  }

  renderTopics(topics) {
    return topics.map(topic => (
      <TopicTile
        key={topic.id}
        topic={topic}
        setActiveTopic={this.props.setActiveTopic}
        editTopic={this.editTopic}
        deleteTopic={this.deleteTopic}
      />
    ));
  }

  handleAddTopicModalClose() {
    this.setState({ isAddTopic: false, editTopic: null });
  }

  renderTopicAddModal() {
    if (this.state.isAddTopic) {
      return (
        <TopicAddModal
          onClick={() => this.handleAddTopicModalClose()}
          topic={this.state.editTopic}
        />
      );
    }
  }

  handleDeleteTopicModalClose() {
    this.setState({ isDeleteTopic: false, editTopic: null });
  }

  handleDeleteTopic(topic) {
    this.props.deleteTopic(topic);
    this.setState(
      { isDeleteTopic: false, editTopic: null },
      this.props.notify({ type: "danger", text: "Topic Deleted!" })
    );
  }

  getTopicTasks(topicId) {
    return (
      this.props.tasksList &&
      this.props.tasksList.filter(
        task => task.id_project_section_topic === topicId
      )
    );
  }

  renderDeleteTopicModal() {
    if (this.state.isDeleteTopic) {
      const topicTasks = this.getTopicTasks(this.state.editTopic.id);
      return (
        <TopicDeleteModal
          onDismiss={() => this.handleDeleteTopicModalClose()}
          topic={this.state.editTopic}
          onDelete={() => {
            this.handleDeleteTopic(this.state.editTopic);
          }}
          topicTasks={topicTasks}
        />
      );
    }
  }

  handleDeleteSection(section) {
    this.props.deleteSection(section);
    this.props.notify({ type: "danger", text: "Section Deleted!" });
  }

  renderDeleteSectionButton(section, { sectionName }) {
    const hasTopics =
      this.props.topics.topicsList &&
      this.props.topics.topicsList.find(
        t => t.id_project_section === section.id
      );
    const isSectionSelected = sectionName === section.name;
    return isSectionSelected && !hasTopics ? (
      <div className="columns column">
        <div className="column horizontal-line" />
        <div className="column is-1">
          <a
            className="icon"
            onClick={e => this.handleDeleteSection(section)}
            style={{ zIndex: 10 }}
          >
            <span className="icon has-text-grey">
              <i className="fa fa-remove" />
            </span>
          </a>
        </div>
      </div>
    ) : null;
  }

  renderSections(sectionsList) {
    return sectionsList.map(section => {
      return (
        <div key={section.id} className="ds-section-container">
          <form
            className="field ds-section-title"
            onSubmit={e => {
              e.preventDefault();
              this.props.updateSection({
                id: section.id,
                name: this.state.sectionName,
                num_row: section.num_row
              });
              this.refs[section.id].blur();
            }}
          >
            <div className="control columns">
              <input
                className="input is-uppercase column is-three-fifths"
                ref={section.id}
                type="text"
                defaultValue={section.name}
                onChange={e => {
                  this.setState({ sectionName: e.target.value });
                }}
                onClick={e => {
                  this.setState({ sectionName: e.target.value });
                }}
                onBlur={e => {
                  setTimeout(() => {
                    if (this.state.sectionName === section.name) {
                      this.setState({ sectionName: "" });
                    }
                  }, 100);
                }}
              />
              {this.renderDeleteSectionButton(section, this.state)}
            </div>
          </form>
          <div className="tile is-ancestor">
            {this.props.topics.topicsList.map(topic => {
              if (section.id === topic.id_project_section) {
                return (
                  <TopicTile
                    key={topic.id}
                    topic={topic}
                    setActiveTopic={this.props.setActiveTopic}
                    editTopic={this.editTopic}
                    deleteTopic={this.deleteTopic}
                    topicTasks={this.getTopicTasks(topic.id)}
                  />
                );
              } else return null;
            })}
          </div>
        </div>
      );
    });
  }

  render() {
    if (!this.props.topics.topicsList) {
      return <Loader />;
    }

    return (
      <section className="section ds-content ds-task-management">
        <a
          className="button is-large ds-add-topic"
          onClick={() => {
            this.setState({ isAddTopic: true });
          }}
        >
          <span className="icon is-medium">
            <i className="fa fa-plus-square-o" />
          </span>
          <span className="is-uppercase">Add Topic</span>
        </a>
        {this.renderTopicAddModal()}
        {this.renderDeleteTopicModal()}
        {this.renderSections(this.props.sectionsList)}
        <form
          className="field ds-section-title ds-add-section"
          onSubmit={e => {
            e.preventDefault();
            this.props.addSection({
              id_project: this.props.activeProject.id,
              name: this.state.newSectionName
            });
            this.setState({ newSectionName: "" });
          }}
          style={{ opacity: this.state.isAddFocus ? "1" : "" }}
        >
          <div className="control has-icons-left">
            <input
              className="input is-large is-uppercase"
              ref="newSection"
              type="text"
              placeholder="ADD NEW SECTION"
              value={this.state.newSectionName}
              onChange={e => {
                this.setState({ newSectionName: e.target.value });
              }}
              onBlur={() => {
                this.setState({ isAddFocus: false });
              }}
              onFocus={() => {
                this.setState({ isAddFocus: true });
              }}
            />
            <span className="icon is-large is-left">
              <i className="fa fa-plus-square-o" />
            </span>
          </div>
        </form>
      </section>
    );
  }
}

function mapStateToProps(state) {
  return {
    topics: state.topics,
    activeProject: state.projects.activeProject,
    sectionsList: state.sections.sectionsList,
    tasksList: state.tasks.tasksList
  };
}

export default connect(mapStateToProps, actions)(TaskManagement);
