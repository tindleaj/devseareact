import React from 'react';
import { Link } from 'react-router-dom';

export default function TopicTile(props) {
  function textTruncate(str, length, ending) {
    if (str.length === 0) return null;
    if (length == null) {
      length = 18;
    }
    if (ending == null) {
      ending = '...';
    }
    if (str.length > length) {
      return str.substring(0, length) + ending;
    } else {
      return str;
    }
  }

  const renderTasksSkills = (tasks = []) => {
    const skills = tasks.reduce((acc, e) => acc.concat(e.skills), []);
    if (!skills.length) {
      return null;
    }
    const mostPopular = getMostPopularSkills(skills);
    return mostPopular.map(e => (
      <a key={`skill-${e.name}`} className="button is-outlined is-info is-small">
        {e.name} ({e.count})
      </a>
    ));
  };

  // Compute most popular skills in topic via skills in each task
  const getMostPopularSkills = skills => {
    return skills
      .reduce((acc, s, i, arr) => {
        const obj = {
          ...s,
          count: arr.filter(e => s.id === e.id).length,
        };
        if (!acc.find(s => s.id === obj.id)) {
          acc.push(obj);
        }
        return acc;
      }, [])
      .sort((a, b) => b.count - a.count)
      .slice(0, 4);
  };

  return (
    <div className="tile is-parent is-4 hvr-float">
      <div className="tile is-child box">
        <Link to={`/app/topic/${props.topic.id}`}>
          <div className="level is-mobile ds-points">
            <div className="level-left">
              <p className="is-size-4 ds-points-number">
                {props.topic.totalPoints || '-'}
              </p>
              <p>&nbsp;points</p>
            </div>
            <div className="level-right">
              <a
                className="icon">
                <i className="fa fa-info-circle has-text-primary" />
              </a>
            </div>
          </div>
          <div>
            <h2 className="title is-4 has-text-grey-dark">
              {textTruncate(props.topic.name)}
            </h2>
          </div>
          <p className="content">{props.topic.description || 'N/A'}</p>
          <span className="ds-topic-tile-skills">
            {renderTasksSkills(props.topicTasks)}
          </span>
        </Link>
        <br />
        <div className="level ds-topic-edit-buttons">
          <div className="level-left" />
          <div className="level-right">
            <a
              className="icon"
              onClick={() => props.editTopic(props.topic)}
              style={{ zIndex: 10 }}
            >
              <i className="fa fa-edit" />
            </a>
            <a
              className="icon"
              onClick={() => props.deleteTopic(props.topic)}
              style={{ zIndex: 10 }}
            >
              <i className="fa fa-remove" />
            </a>
          </div>
        </div>
      </div>
    </div>
  );
}
