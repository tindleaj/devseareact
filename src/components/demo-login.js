import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import Loader from './loader';
import auth from '../../config/auth';

import * as actions from '../actions';

const DemoLogin = props => {
	if (!auth.demoToken || auth.demoToken === '')
		return (
			<section className="section ds-content">
				<h1>No demo token</h1>
			</section>
		);

	localStorage.setItem('token', auth.demoToken);

	props.authUser();

	if (!props.authenticated)
		return (
			<section className="section ds-content">
				<Loader />
			</section>
		);

	return <Redirect to="/app" />;
};

function mapStateToProps(state) {
	return {
		authenticated: state.auth.authenticated,
	};
}

export default connect(mapStateToProps, actions)(DemoLogin);
