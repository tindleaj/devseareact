import React from 'react';

const ProjectItem = props =>
  <a onClick={() => props.clickHandler(props.project)} className="dropdown-item">
    {props.project.name}
  </a>;

export default ProjectItem;
