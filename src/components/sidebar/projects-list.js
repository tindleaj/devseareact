import React, { Component } from 'react';
// Little helper library for assigning classnames
import cn from 'classnames';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import * as actions from '../../actions';

import ProjectItem from './project-item';

class ProjectsList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      dropdownIsActive: false
    };
  }

  // Set the state of the dropdown, update css classes accordingly
  dropdownToggle() {
    this.setState({ dropdownIsActive: !this.state.dropdownIsActive });
  }

  handleProjectItemClick(project) {
    this.props.setActiveProject(project);
    this.props.history.push('/app/task-management');
    this.dropdownToggle();
  }

  renderDropdown() {
    return null;
  }

  render() {
    const classes = cn('dropdown', {
      'is-active': this.state.dropdownIsActive
    });

    return (
      <div className="ds-projects-list">
        <div className={classes}>
          <div className="dropdown-trigger">
            <a className="level" onClick={() => this.dropdownToggle()}>
              <span className="is-size-5 level-left is-hidden-touch">
                {this.props.activeProject
                  ? this.props.activeProject.name
                  : 'Loading...'}
              </span>
              <span className="icon level-right">
                <i className="fa fa-angle-down" />
              </span>
            </a>
          </div>
          <div className="dropdown-menu">
            <div className="dropdown-content">{this.renderDropdown()}</div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    projectsList: state.projects.projectsList,
    activeProject: state.projects.activeProject
  };
}

export default withRouter(connect(mapStateToProps, actions)(ProjectsList));
