import React from "react";
import { Link } from "react-router-dom";

const MenuList = () => (
  <ul className="ds-menu-list">
    <li>
      <Link to="/app/project-overview" className="level">
        <span className="icon level-left">
          <i className="fa fa-drivers-license-o" />
        </span>
        <span className="level-item is-hidden-touch">Project Overview</span>
      </Link>
    </li>
    <li>
      <Link to="/app/task-management" className="level">
        <span className="icon level-left">
          <i className="fa fa-tasks" />
        </span>
        <span className="level-item is-hidden-touch">Task Management</span>
      </Link>
    </li>
    <li>
      <a className="level">
        <span className="icon level-left">
          <i className="fa fa-calendar-plus-o" />
        </span>
        <span className="level-item is-hidden-touch">Sprint Management</span>
      </a>
    </li>
    <li>
      <Link to="/app/leaderboard" className="level">
        <span className="icon level-left">
          <i className="fa fa-trophy" />
        </span>
        <span className="level-item is-hidden-touch">Leaderboard</span>
      </Link>
    </li>
    <li>
      <a className="level">
        <span className="icon level-left">
          <i className="fa fa-user-plus" />
        </span>
        <span className="level-item is-hidden-touch">Recruiting</span>
      </a>
    </li>
    <li>
      <a className="level">
        <span className="icon level-left">
          <i className="fa fa-gavel" />
        </span>
        <span className="level-item is-hidden-touch">Legal</span>
      </a>
    </li>
  </ul>
);

export default MenuList;
