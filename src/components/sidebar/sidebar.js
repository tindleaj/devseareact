import React, { Component } from "react";
import { connect } from "react-redux";

import ProjectsList from "./projects-list";
import MenuList from "./menu-list";

import logoIcon from "../../img/logo-icon.png";

class Sidebar extends Component {
  render() {
    if (this.props.auth.authenticated && !this.props.auth.hideSidebar) {
      return (
        <aside className="menu ds-sidebar column is-paddingless">
          <div className="ds-sidebar-logo level is-mobile">
            <div className="level-left">
              <figure className="image is-square ds-logo">
                <img src={logoIcon} alt="Devsea logo" />
              </figure>
            </div>
            <div className="level-item is-hidden-touch">
              <h2 className="is-uppercase is-size-3">devsea</h2>
            </div>
          </div>
          <ProjectsList />
          <MenuList />
        </aside>
      );
    }
    return null;
  }
}

function mapStateToProps(state) {
  return {
    auth: state.auth
  };
}
export default connect(mapStateToProps)(Sidebar);
