import React from 'react';

const LeaderboardHeaderRow = props => (
	<div className="columns box ds-leaderboard-header is-size-7">
		<span className="column rank">Rank</span>
		<span className="column contributor">Contributor</span>
		<span className="column">Points in play</span>
		<span className="column">Points earned</span>
		<span className="column">Tasks done</span>
		<span className="column">Contribution (%)</span>
	</div>
);

export default LeaderboardHeaderRow;