import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions/index';

import LeaderboardRow from './leaderboard-row';
import LeaderboardHeaderRow from './leaderboard-header-row';

class Leaderboard extends Component {
	componentWillMount() {
		this.props.fetchActiveProjectLeaderboard(this.props.activeProject.id);
	}

	renderLeaderboardRows(leaderboard) {
		return leaderboard.contributors.map((user, index) => {
			return (
				<LeaderboardRow user={user} index={index} totalPoints={leaderboard.totalPoints} key={index} />
			);
		});
	}

	render() {
		return (
			<section className="section ds-content">
  			<h4 className="title is-4 ds-component-title">Leaderboard</h4>
				<div className="ds-leaderboard">
					<LeaderboardHeaderRow />
					{this.props.leaderboard ? this.renderLeaderboardRows(this.props.leaderboard) : 'loading'}
				</div>
			</section>
		);
	}
}

function mapStateToProps(state) {
  return {
    activeProject: state.projects.activeProject,
    leaderboard: state.projects.leaderboard,
    tasksList: state.tasks.tasksList,
  };
}

export default connect(mapStateToProps, actions)(Leaderboard);