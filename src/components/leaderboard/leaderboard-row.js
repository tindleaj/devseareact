import React from 'react';

const LeaderboardRow = props => (
	<div className="columns box">
		<span className="column has-border-right rank">{props.index + 1}</span>
		<span className="column level contributor is-marginless">
			<p className="level-left image is-32x32">
				<img
					src={`https://lorempixel.com/200/200/people/${props.index}`}
					alt="Profile"
				/>
			</p>
			<span className="level-item">
				<strong>{props.user.username}</strong>
			</span>
		</span>
		<span className="column">
			{props.user.pointsPool - props.user.pointsAwarded}
		</span>
		<span className="column">{props.user.pointsAwarded}</span>
		<span className="column">{props.user.tasksCompleted}</span>
		<span className="column">
			{(props.user.pointsAwarded / props.totalPoints * 100).toFixed(1)}%
		</span>
	</div>
);

export default LeaderboardRow;
