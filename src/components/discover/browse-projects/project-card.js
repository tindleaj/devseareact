import React from 'react';
import { Link } from 'react-router-dom';

export default function ProjectCard(props) {
	return (
		<div className="column is-half">
			<div className="card">
				<div className="card-image">
					<figure className="image">
						<img
							src={`https://picsum.photos/600/400?image=${props.project.id}`}
							alt="Project"
						/>
					</figure>
				</div>
				<div className="card-content">
					<h3 className="title">{props.project.name}</h3>
					<h4
						className="subtitle is-6 has-text-grey"
						style={{ marginTop: '-1rem' }}
					>{`by ${props.project.name}`}</h4>
					<p>{props.project.bio}</p>
					<br />
					<progress
						className="progress is-success is-medium"
						value="60"
						max="100"
						style={{ width: '100%' }}
					>
						60%
					</progress>
					<div className="is-size-9 has-text-grey">
						30%&nbsp;&nbsp;|&nbsp;&nbsp;$1,000&nbsp;&nbsp;|&nbsp;&nbsp;Tasks completed:
						30/47
					</div>
					<br />
					<Link
						className="button is-info is-fullwidth"
						to="/app/task-management"
						onClick={() => props.setActiveProject(props.project)}
					>
						View Project
					</Link>
				</div>
			</div>
		</div>
	);
}
