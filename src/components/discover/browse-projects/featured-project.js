import React from "react";
import { Link } from "react-router-dom";

export default function FeaturedProject(props) {
  return (
    <div className="column">
      <div className="card">
        <div className="card-image">
          <figure className="image">
            <img
              src={`https://picsum.photos/1000/500?image=${props.project.id}`}
              alt="Project"
            />
          </figure>
        </div>
        <div className="card-content">
          <h3 className="title">{props.project.name}</h3>
          <h4
            className="subtitle has-text-grey is-6"
            style={{ marginTop: "-1rem" }}
          >
            {`by ${props.project.name}`}
          </h4>
          <p>{props.project.bio}</p>
          <br />

          <div className="columns">
            <div className="column">
              <article>
                <progress
                  className="progress is-success"
                  value={props.tasks.completed}
                  max={props.tasks.total}
                >
                  {(props.tasks.completed / props.tasks.total * 100).toFixed()}%
                </progress>
                <span className="ds-progress-text">
                  Tasks completed: {props.tasks.completed}/{props.tasks.total}
                </span>
              </article>
            </div>

            <div className="column is-half">
              <Link
                className="button is-info is-fullwidth"
                to="/app/task-management"
                onClick={() => props.setActiveProject(props.project)}
              >
                View Project
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
