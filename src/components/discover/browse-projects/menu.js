import React from 'react';

export default props => {
	return(
		<aside className="menu">
		  <p className="menu-label">
		    Project Type
		  </p>
		  <ul className="menu-list">
		    <li><a>All projects</a></li>
		    <li><a>Startups</a></li>
		    <li><a>Community</a></li>
		    <li><a>Enterprise</a></li>
		  </ul>
		  <p className="menu-label">
		    Compensation Type
		  </p>
		  <ul className="menu-list">
		    <li><a>All compensation</a></li>
		    <li><a>Cash</a></li>
		    <li><a>Equity</a></li>
		    <li><a>Other</a></li>
		  </ul>
		  <p className="menu-label">
		    Skills Required
		  </p>
		  <ul className="menu-list">
		    <li><a>All Skills</a></li>
		    <li><a>Full-Stack</a></li>
		    <li><a>Front-End</a></li>
		    <li><a>Backend</a></li>
		    <li><a>Data Science</a></li>
		    <li><a>Artificial Intelligence</a></li>
		    <li><a>Blockchain</a></li>
		    <li><a>UX/UI Design</a></li>
		    <li><a>Graphic Design</a></li>
		    <li><a>Marketing</a></li>
		    <li><a>Social Media</a></li>
		    <li><a>Writing</a></li>
		  </ul>
		  <p className="menu-label">
		    Status
		  </p>
		  <ul className="menu-list">
		    <li><a>All status</a></li>
		    <li><a>Just launched</a></li>
		    <li><a>Ending soon</a></li>
		    <li><a>Upcoming</a></li>
		  </ul>
		</aside>
	);
}