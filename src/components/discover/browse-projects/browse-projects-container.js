import React, { Component } from "react";
import { connect } from "react-redux";

import Hero from "./hero";
import Menu from "./menu";
import FeaturedProject from "./featured-project";
import ProjectCard from "./project-card";

import * as actions from "../../../actions";

class BrowseProjects extends Component {
  renderProjectCards() {
    for (let i = 1; i < this.props.projectsList.length; i++) {
      return (
        <ProjectCard
          project={this.props.projectsList[i]}
          key={i}
          activeProject={this.props.activeProject}
          tasks={this.computeTasks(this.props.tasksList)}
        />
      );
    }
  }

  computeTasks(tasks) {
    let total = tasks.length;
    let completed = tasks.filter(task => {
      return task.finished;
    }).length;

    return {
      total,
      completed
    };
  }

  render() {
    return (
      <div>
        <Hero user={this.props.me} />
        <br />
        <section className="ds-content-no-sidebar section">
          <div className="container">
            <div className="columns">
              <div className="column is-one-quarter">
                <Menu />
              </div>
              <div className="column is-three-quarters">
                <div className="columns">
                  <FeaturedProject
                    project={this.props.projectsList[0]}
                    activeProject={this.props.activeProject}
                    tasks={this.computeTasks(this.props.tasksList)}
                  />
                </div>
                <br />

                <div className="columns">{this.renderProjectCards()}</div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    projectsList: state.projects.projectsList,
    me: state.users.me,
    activeProject: state.projects.activeProject,
    tasksList: state.tasks.tasksList
  };
}

export default connect(mapStateToProps, actions)(BrowseProjects);
