import React from "react";

export default props => {
  return (
    <header
      className="hero is-medium"
      style={{
        background: "linear-gradient(to bottom right, #1AA094, #179286)"
      }}
    >
      <div className="container">
        <div className="hero-body column is-two-thirds">
          <h2 className="title is-2 has-text-light">
            <p>{`Hi ${props.user.firstname}! Welcome to Devsea. `}</p>
            <p>
              <a className="has-text-weight-bold has-text-white">Explore</a>{" "}
              awesome projects.{" "}
            </p>
            <p>
              Or,{" "}
              <a className="has-text-weight-bold has-text-white">
                start your own.
              </a>
            </p>
          </h2>
        </div>
      </div>
    </header>
  );
};
