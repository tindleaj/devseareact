import React, { Component } from 'react';

//TODO: This needs to be made dynamic using info in backend
const workloadPointsMap = {
  1: 50,
  2: 100,
  3: 200,
  4: 400,
  5: 600,
};

class SetWorkload extends Component {
  constructor(props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);

    this.state = {
      id_workload: this.props.defaultWorkload || '',
      points: workloadPointsMap[this.props.defaultWorkload],
    };
  }

  handleChange() {
    const workload = this.refs.workload.value;

    this.setState({
      id_workload: parseInt(workload, 10),
      points: workloadPointsMap[workload],
    });
  }

  render() {
    return (
      <div>
        <div className="field">
          <label htmlFor="workload" className="label is-size-4">
            Select workload to determine due date.
          </label>
          <div className="control">
            <div className="select">
              <select
                ref="workload"
                onChange={this.handleChange}
                value={this.state.id_workload}
                disabled={this.props.defaultWorkload !== null}
              >
                <option value={null}>Set workload</option>
                <option value={1}>Nano (0-3 hours)</option>
                <option value={2}>Superlight (3-6 hours)</option>
                <option value={3}>Light (6-12 hours)</option>
                <option value={4}>Medium (12-24 hours)</option>
                <option value={5}>Heavy (24-36 hours)</option>
              </select>
            </div>
          </div>
        </div>
        <div className="field">
          <div className="control">
            <a
              onClick={
                this.state.id_workload === '' || !this.state.id_workload
                  ? null
                  : () => {
                      this.props.nextStep(this.state);
                    }
              }
              className="button is-fullwidth is-info is-size-9 button-continue"
              disabled={
                this.state.id_workload === '' || !this.state.id_workload
              }
            >
              CONTINUE
            </a>
          </div>
        </div>
      </div>
    );
  }
}

export default SetWorkload;
