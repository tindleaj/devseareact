import React, { Component } from "react";

import TaskAddModalForm from "./task-add-modal-form";

class TaskAddModal extends Component {
  render() {
    return (
      <div className="ds-task-add-modal modal is-active">
        <div
          className={`modal-background ${
            !this.props.task ? "" : "has-no-background"
          }`}
          onClick={() => this.props.onClick()}
        />
        <div className="modal-card">
          <header className="modal-card-head level is-marginless section">
            <p className="level-left modal-card-title is-uppercase">
              {!this.props.task ? "Add task" : "Edit task"}
            </p>
            <div className="level-right" />
          </header>
          <section className="modal-card-body section">
            <TaskAddModalForm
              onClick={() => this.props.onClick()}
              task={this.props.task}
            />
          </section>
        </div>
      </div>
    );
  }
}

export default TaskAddModal;
