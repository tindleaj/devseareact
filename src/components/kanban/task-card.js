import React from 'react';

export default props => <div className="ds-task-card">{props.metadata.TaskName || 'N/A'}</div>;
