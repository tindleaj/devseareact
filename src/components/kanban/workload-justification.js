import React, { Component } from 'react';

class WorkloadJustification extends Component {
  constructor(props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);

    this.state = {
      justification: ''
    };
  }

  handleChange() {
    const justification = this.refs.justification.value;

    this.setState({ justification });
  }

  render() {
    return (
      <div>
        <div className="field">
          <label htmlFor="justification" className="label is-size-4">
            Enter workload justification
          </label>
          <div className="control">
            <textarea
              className="textarea"
              onChange={this.handleChange}
              ref="justification"
              placeholder="(optional)"
            />
          </div>
        </div>
        <div className="field">
          <div className="control">
            <a
              onClick={() => this.props.nextStep(this.state)}
              className="button is-fullwidth is-info is-size-9 button-continue"
            >
              CONTINUE
            </a>
          </div>
        </div>
      </div>
    );
  }
}

export default WorkloadJustification;
