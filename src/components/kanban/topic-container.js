import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import TaskModal from './task-modal';
import Kanban from './kanban';
import TaskAddModal from './task-add-modal';
import Loader from '../loader';

import * as actions from '../../actions';

class TopicContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isAddTask: false,
      editTask: null,
    };
  }

  componentDidMount() {
    this.props.fetchSkills();
  }

  componentWillMount() {
    this.props.setActiveTopic(this.props.match.params.topicId);
  }

  componentWillUnmount() {
    this.props.setActiveTask(null);
  }

  handleCloseAddModal() {
    this.setState({ isAddTask: false, editTask: null });
  }

  renderModal(task) {
    if (task)
      return (
        <TaskModal
          task={task}
          editTask={this.editTask.bind(this)}
          setActiveTask={this.props.setActiveTask}
        />
      );
  }

  editTask(task) {
    this.setState({ isAddTask: true, editTask: task });
  }

  renderTaskAddModal(isAddTask) {
    if (isAddTask) {
      return (
        <TaskAddModal
          onClick={() => this.handleCloseAddModal()}
          notify={this.props.notify}
          deleteTask={this.props.deleteTask}
          task={this.state.editTask || null}
        />
      );
    }
  }

  render() {
    if (!this.props.activeTopic || !this.props.tasksList) {
      return <Loader />;
    }

    return (
      <section className="section ds-content">
        <div className="columns">
          <div className="column is-1" style={{ width: '5%' }}>
            <Link to="/app/task-management" className="icon has-text-grey">
              <i className="fa fa fa-arrow-left" />
            </Link>
          </div>
          <div className="column">
            <h2 className="title is-4 ds-component-title">
              {this.props.activeTopic.name}
            </h2>
            <p className="has-text-grey">
              {this.props.activeTopic.description || 'No description'}
            </p>
          </div>
        </div>

        <br />

        {this.renderTaskAddModal(this.state.isAddTask)}
        {this.renderModal(this.props.activeTask)}

        <Kanban
          isAddTask={() => {
            this.setState({ isAddTask: true });
          }}
          tasksList={this.props.tasksList.filter(task => {
            return (
              task.id_project_section_topic ===
              parseInt(this.props.match.params.topicId, 10)
            );
          })}
          isAdmin={this.props.me.id_role === 2}
        />
      </section>
    );
  }
}

function mapStateToProps(state) {
  return {
    me: state.users.me,
    activeTopic: state.topics.activeTopic,
    tasksList: state.tasks.tasksList,
    activeTask: state.tasks.activeTask,
    topicsList: state.topics.topicsList,
  };
}

export default connect(mapStateToProps, actions)(TopicContainer);
