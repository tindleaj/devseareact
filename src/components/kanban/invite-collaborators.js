import React, { Component } from 'react';
import Select from 'react-select';

class InviteCollaborators extends Component {
  constructor(props) {
    super(props);

    this.state = {
      users: props.defaultUsers || [],
      availablePoints: props.points,
    };
  }

  handleAddCollaborator() {
    if (this.state.user === null || this.refs.points.value === '')
      return this.setState({ message: 'You must include a username and a points value' });

    if (parseInt(this.refs.points.value, 10) > this.state.availablePoints)
      return this.setState({
        message: 'You cannot assign a collaborator more points than the task has available',
      });

    const foundUser = this.props.usersList.find(user => user.id === this.state.id_user);

    if (!foundUser)
      return this.setState({
        message: 'No user with that username found in this project',
      });

    const user = {
      id_user: foundUser.id,
      name: `${foundUser.firstname} ${foundUser.lastname}`,
      id_task_role: 2,
      responsibility: this.refs.responsibility.value,
      points: this.refs.points.value,
    };

    this.setState({
      id_user: null,
      availablePoints: this.state.availablePoints - parseInt(this.refs.points.value, 10),
      users: [...this.state.users, user],
      message: '',
    });

    this.refs.responsibility.value = '';
    this.refs.points.value = '';
  }

  renderCollaborators(users) {
    return users.map((user, index) => {
      return (
        <div className="field" key={index}>
          <div className="control">
            <input
              className="input"
              type="text"
              value={`${user.name} - ${user.points} points`}
              disabled
            />
          </div>
        </div>
      );
    });
  }

  handleUsername(selectedOption) {
    this.setState({ id_user: selectedOption ? selectedOption.value : null });
  }

  render() {
    return (
      <div style={{ marginBottom: '5rem' }}>
        <div className="field">
          <label htmlFor="finish" className="label is-size-4">
            Invite collaborators
          </label>
          <label htmlFor="availablePoints" className="label is-size-6">
            Available Points: {this.state.availablePoints}
          </label>
          <div className="control">
            <Select
              name="id_user"
              value={this.state.id_user}
              onChange={this.handleUsername.bind(this)}
              options={this.props.usersList.map(user => ({
                value: user.id,
                label: `${user.username} - ${user.firstname} ${user.lastname || ''}`,
              }))}
            />
          </div>
        </div>

        <div className="field">
          <div className="control">
            <textarea
              className="textarea"
              ref="responsibility"
              placeholder="Insert collaborator's responsibility"
            />
          </div>
        </div>

        <div className="field">
          <div className="control">
            <input className="input" type="text" ref="points" placeholder="Insert points" />
          </div>
        </div>

        <div className="field">
          <div className="control">
            <a
              onClick={() => {
                this.handleAddCollaborator();
              }}
              className="button is-fullwidth is-info is-size-9 button-continue"
            >
              ADD COLLABORATOR
            </a>
          </div>
          <p className="help is-danger">{this.state.message}</p>
        </div>

        {this.renderCollaborators(this.state.users)}

        <div className="field">
          <div className="control">
            <a
              onClick={() => this.props.submitForm(this.state.users)}
              className="button is-fullwidth is-success is-size-9 button-continue"
            >
              FINISH
            </a>
          </div>
        </div>
      </div>
    );
  }
}

export default InviteCollaborators;
