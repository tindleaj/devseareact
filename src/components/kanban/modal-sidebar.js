import React, { Component } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';

import * as actions from '../../actions';

import StepCount from './step-count';

import SetWorkload from './set-workload';
import WorkloadJustification from './workload-justification';
import AssignTask from './assign-task';
import InviteCollaborators from './invite-collaborators';
import InfoPanel from './info-panel';

class ModalSidebar extends Component {
  constructor(props) {
    super(props);

    this.nextStep = this.nextStep.bind(this);
    this.setStep = this.setStep.bind(this);
    this.handleFormSubmit = this.handleFormSubmit.bind(this);

    let startingStep = 0;

    if (props.activeTask.id_workload && props.activeTask.id_user_owner)
      startingStep = 4;

    this.state = {
      step: startingStep
    };
  }

  setStep(step) {
    this.setState({
      step
    });
  }

  nextStep(newFields) {
    this.setState({
      fields: {
        ...this.state.fields,
        ...newFields
      },
      step: this.state.step + 1
    });
  }

  handleFormSubmit(collaborators) {
    const newTask = {
      id: this.props.activeTask.id,
      TaskName: this.props.activeTask.TaskName,
      description: this.props.activeTask.description,
      ...this.state.fields,
      date_started: moment().format('YYYY-MM-DD'),
      num_col: 1
    };

    collaborators.forEach(collaborator => {
      this.props.addTaskUser(this.props.activeTask.id, collaborator);
    });

    this.props.updateTask(newTask);

    this.nextStep();
  }

  renderStep() {
    switch (this.state.step) {
      case 0:
        return (
          <AssignTask
            nextStep={this.nextStep}
            me={this.props.me}
            usersList={this.props.usersList}
          />
        );
      case 1:
        return (
          <SetWorkload
            nextStep={this.nextStep}
            defaultWorkload={this.props.activeTask.id_workload}
          />
        );
      case 2:
        return <WorkloadJustification nextStep={this.nextStep} />;
      case 3:
        return (
          <InviteCollaborators
            points={this.state.fields.points}
            usersList={this.props.usersList.filter(
              user => user.id !== this.props.me.id
            )}
            nextStep={this.nextStep}
            submitForm={this.handleFormSubmit}
          />
        );
      case 4:
        return (
          <InfoPanel task={this.props.activeTask} setStep={this.setStep} />
        );
      default:
    }
  }

  render() {
    return (
      <div className="ds-task-modal-options column is-4">
        <StepCount step={this.state.step} setStep={this.setStep} />
        {this.renderStep()}
        <div className="options-buttons">
          <a className="button is-large">
            <span className="icon is-medium">
              <i className="fa fa-columns" />
            </span>
          </a>
          <a
            href={this.props.activeTask.url_github}
            className="button is-large"
          >
            <span className="icon is-medium">
              <i className="fa fa-github" />
            </span>
          </a>
          <a className="button is-large">
            <span className="icon is-medium">
              <i className="fa fa-share" />
            </span>
          </a>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    me: state.users.me,
    usersList: state.users.usersList,
    activeTask: state.tasks.activeTask
  };
}

export default connect(mapStateToProps, actions)(ModalSidebar);
