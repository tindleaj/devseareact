import React, { Component } from 'react';
import {
	SortableContainer,
	SortableElement,
	arrayMove
} from 'react-sortable-hoc';
import { connect } from 'react-redux';
import _ from 'lodash';

import * as actions from '../../actions';

const SortableItem = SortableElement(({ task, setActiveTask, isDisabled }) => {
	return (
		<li
			onClick={() => {
				if (!isDisabled) setActiveTask(task);
			}}
		>
			<div
				className={`ds-kanban-box box ${
					isDisabled ? 'ds-kanban-box-disabled' : ''
				}`}
			>
				<p className="has-text-grey">{task.TaskName}</p>
			</div>
		</li>
	);
});

const SortableList = SortableContainer(({ items, setActiveTask, disabled }) => {
	return (
		<ul>
			{items.map((task, index) => (
				<SortableItem
					key={`item-${task.id}`}
					index={index}
					task={task}
					setActiveTask={setActiveTask}
					isDisabled={disabled}
				/>
			))}
		</ul>
	);
});

class KanbanCol extends Component {
	state = {
		items: this.sortItems(this.props.items)
	};

	onSortEnd = ({ oldIndex, newIndex }) => {
		const newTask = this.state.items[oldIndex];

		this.setState({
			items: arrayMove(this.state.items, oldIndex, newIndex)
		});

		let before = this.state.items[newIndex - 1];
		let after = this.state.items[newIndex + 1];

		before = before ? parseFloat(before.num_row) : 0;
		after = after ? parseFloat(after.num_row) : before + 5000;

		newTask.num_row = (before + after) / 2;

		this.props.updateTask(
			_.pick(newTask, ['id', 'TaskName', 'description', 'num_row'])
		);
	};

	componentWillReceiveProps(props) {
		this.setState({
			items: this.sortItems(props.items)
		});
	}

	sortItems(items) {
		return items.sort((a, b) => {
			return a.num_row - b.num_row;
		});
	}

	renderAdd(hasAdd) {
		if (hasAdd) {
			return (
				<div className="ds-add-task-col">
					<a
						className="button is-fullwidth"
						onClick={() => {
							this.props.isAddTask();
						}}
					>
						<span className="icon is-small">
							<i className="fa fa-plus-square" />
						</span>
						<span className="is-uppercase">Add New Task</span>
					</a>
				</div>
			);
		}

		return null;
	}

	render() {
		return (
			<div className="ds-kanban-col">
				<h5 style={{ backgroundColor: this.props.color || '#2ed495' }}>
					{this.props.title}
				</h5>
				<div>
					<SortableList
						items={this.state.items}
						onSortEnd={this.onSortEnd}
						lockToContainerEdges={true}
						distance={5}
						helperClass="ds-kanban-helper"
						setActiveTask={this.props.setActiveTask}
						disabled={this.props.disabled}
					/>
				</div>
				{this.renderAdd(this.props.hasAdd)}
			</div>
		);
	}
}

export default connect(null, actions)(KanbanCol);
