import React, { Component } from 'react';
import KanbanCol from './kanban-col.js';

class Kanban extends Component {
  componentWillReceiveProps(props) {
    this.setState({
      tasksList: props.tasksList
    });
  }

  render() {
    const tasksList = this.props.tasksList;

    function filterTasks(tasksList, column) {
      return tasksList.filter(task => {
        return parseInt(task.num_col, 10) === column;
      });
    }

    return (
      <div className="ds-kanban">
        <KanbanCol
          title="To Do"
          color="#2ed495"
          items={filterTasks(tasksList, 0)}
          isAddTask={this.props.isAddTask}
          hasAdd={true}
        />
        <KanbanCol
          title="Work In Progress"
          color="#2DCBBE"
          items={filterTasks(tasksList, 1)}
        />
        <KanbanCol
          title="In Review"
          color="#29C0D9"
          items={filterTasks(tasksList, 2)}
        />
        <KanbanCol
          title="Completed"
          color="#1B8EF2"
          items={filterTasks(tasksList, 3)}
          disabled={!this.props.isAdmin}
        />
      </div>
    );
  }
}

export default Kanban;
