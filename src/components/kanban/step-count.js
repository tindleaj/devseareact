import React, { Component } from 'react';

class StepCount extends Component {
  render() {
    if (this.props.step === 0) return <div className="section" />;
    if (this.props.step === 4) return null;

    return (
      <nav className="breadcrumb is-centered has-arrow-separator" aria-label="breadcrumbs">
        <ul>
          <li className={this.props.step === 1 ? 'is-active' : ''}>
            <a
              onClick={() => {
                this.props.setStep(1);
              }}
            >
              1
            </a>
          </li>
          <li className={this.props.step === 2 ? 'is-active' : ''}>
            <a
              onClick={() => {
                this.props.setStep(2);
              }}
            >
              2
            </a>
          </li>
          <li className={this.props.step === 3 ? 'is-active' : ''}>
            <a
              onClick={() => {
                this.props.setStep(3);
              }}
            >
              3
            </a>
          </li>
        </ul>
      </nav>
    );
  }
}

export default StepCount;
