import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as actions from '../../actions';

import ModalContent from './modal-content';
import ModalSidebar from './modal-sidebar';

class TaskModal extends Component {
  render() {
    return (
      <div className="modal is-active">
        <div
          className="modal-background"
          onClick={() => {
            this.props.setActiveTask(null);
          }}
        />
        <div className="modal-card">
          <section className="modal-card-body columns is-marginless is-paddingless">
            <ModalContent
              task={this.props.task}
              editTask={this.props.editTask}
              setActiveTask={this.props.setActiveTask}
              deleteTask={this.props.deleteTask}
              notify={this.props.notify}
            />
            <ModalSidebar />
          </section>
        </div>
      </div>
    );
  }
}

export default connect(null, actions)(TaskModal);
