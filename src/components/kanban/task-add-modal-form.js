import React from 'react';
import { reduxForm, Field, FieldArray } from 'redux-form';
import { connect } from 'react-redux';
import Select from 'react-select';
import * as actions from '../../actions';

const validate = values => {
  const errors = {};
  if (!values.TaskName) {
    errors.TaskName = 'Task name is required';
  }
  if (!values.description) {
    errors.description = 'Description is required';
  }

  return errors;
};

const proxySkillsToTask = skills =>
  skills.map(s => ({ id: s.value, name: s.label }));

const proxySkillsToReactSelect = skills =>
  skills.map(s => ({ label: s.name, value: s.id, ...s }));

const handleFormSubmit = (props, task) => {
  const skills = proxySkillsToTask(task.skills);
  if (props.task) {
    props.updateTask({ id: props.task.id, ...task, skills });
  } else {
    props.addTask({ ...task, skills });
  }

  props.onClick();
};

const createRenderer = render => ({
  input,
  meta,
  label,
  placeholder,
  ...rest
}) => (
  <div className="field is-horizontal">
    <div className="field-label is-normal">
      <label className="label">{label}</label>
    </div>
    <div className="field-body">
      <div className="field">
        <div className="control is-expanded">
          {render(input, placeholder, meta, rest)}
        </div>
        {meta.error &&
          meta.touched && <p className="help is-danger">{meta.error}</p>}
      </div>
    </div>
  </div>
);

// Renderers for different form components
const RenderInput = createRenderer((input, placeholder, meta) => (
  <input
    {...input}
    placeholder={placeholder}
    className={[meta.error && meta.touched ? 'is-danger' : '', 'input'].join(
      ' ',
    )}
  />
));

const RenderSelect = createRenderer((input, label, meta, { children }) => (
  <div
    className={[
      meta.error && meta.touched ? 'is-danger' : '',
      'select is-fullwidth',
    ].join(' ')}
  >
    <select {...input} className="select">
      {children}
    </select>
  </div>
));

const RenderReactSelect = createRenderer((input, label, meta, { options }) => {
  const props = {
    ...input,
    onBlur: () => {
      input.onBlur(input.value);
    },
    multi: true,
    options,
  };
  return (
    <div className={[meta.error && meta.touched ? 'is-danger' : ''].join(' ')}>
      <Select {...props} />
    </div>
  );
});

const RenderTextArea = createRenderer((input, label, meta) => (
  <textarea
    {...input}
    placeholder={label}
    className={[meta.error && meta.touched ? 'is-danger' : '', 'textarea'].join(
      ' ',
    )}
  />
));

const renderDocuments = ({ fields }) => (
  <div className="field">
    {fields.map((link, index) => (
      <div className="field is-horizontal" key={index}>
        <div className="field-label is-normal" />
        <div className="field-body" key={index}>
          <Field
            name={`${link}.name`}
            type="text"
            label="Document Name"
            component={RenderInput}
          />
          <Field
            name={`${link}.url`}
            type="text"
            label="URL"
            component={RenderInput}
            placeholder="http://example.com"
          />
          <a
            className="button is-danger"
            title="Remove Link"
            onClick={() => fields.remove(index)}
          >
            X
          </a>
        </div>
      </div>
    ))}
    <div className="field is-horizontal">
      <div className="field-label is-normal">
        <label className="label">Documents</label>
      </div>
      <div className="field-body">
        <div className="field">
          <div className="control">
            <a className="button is-info" onClick={() => fields.push({})}>
              Add Document Links
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
);

let TaskAddModalForm = props => {
  const { handleSubmit, submitting, task, skillsList } = props;
  return (
    <form onSubmit={handleSubmit(task => handleFormSubmit(props, task))}>
      <Field name="TaskName" label="Task name" component={RenderInput} />
      <Field
        name="description"
        label="Description"
        component={RenderTextArea}
      />
      <Field
        name="skills"
        label="Add skills"
        options={proxySkillsToReactSelect(skillsList)}
        component={RenderReactSelect}
      />
      <Field name="finished" label="Status" component={RenderSelect}>
        <option value={false}>Open</option>
        <option value={true}>Closed</option>
      </Field>
      <Field
        name="id_workload"
        label="Workload estimation"
        component={RenderSelect}
      >
        <option value={null} />
        <option value={1}>Nano (0-3 hours)</option>
        <option value={2}>Superlight (3-6 hours)</option>
        <option value={3}>Light (6-12 hours)</option>
        <option value={4}>Medium (12-24 hours)</option>
        <option value={5}>Heavy (24-36 hours)</option>
      </Field>
      <Field
        name="workload_justification"
        label="Justification"
        component={RenderTextArea}
      />
      <FieldArray name="documents" component={renderDocuments} />
      <Field name="url_slack" label="Slack link" component={RenderInput} />
      <Field name="url_github" label="Github link" component={RenderInput} />

      <div className="field is-horizontal">
        <div className="field-label is-normal" />
        <div className="field-body">
          <div className="field">
            <button
              action="submit"
              className="button ds-primary-button is-secondary-blue"
              disabled={submitting}
            >
              {!task ? 'Add new task' : 'Update task'}
            </button>
          </div>
        </div>
      </div>
    </form>
  );
};

TaskAddModalForm = reduxForm({
  form: 'addTask',
  validate,
})(TaskAddModalForm);

TaskAddModalForm = connect(
  (state, ownProps) => ({
    activeTopic: state.topics.activeTopic,
    skillsList: state.skills.skillsList,
    initialValues: {
      TaskName: ownProps.task ? ownProps.task.TaskName : null,
      description: ownProps.task ? ownProps.task.description : null,
      finished: ownProps.task ? ownProps.task.finished : false,
      id_workload: ownProps.task ? ownProps.task.id_workload : null,
      workload_justification: ownProps.task
        ? ownProps.task.workload_justification
        : null,
      id_project_section_topic: state.topics.activeTopic.id,
      num_col: ownProps.task ? ownProps.task.num_col : '0',
      documents: ownProps.task ? ownProps.task.documents : [],
      url_slack: ownProps.task ? ownProps.task.url_slack : '',
      url_github: ownProps.task ? ownProps.task.url_github : '',
      skills: ownProps.task
        ? proxySkillsToReactSelect(ownProps.task.skills)
        : [],
    },
  }),
  actions,
)(TaskAddModalForm);

export default TaskAddModalForm;
