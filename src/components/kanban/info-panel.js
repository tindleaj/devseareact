import React, { Component } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';

import * as actions from '../../actions';

class InfoPanel extends Component {
  renderBottom(finished) {
    if (finished) {
      return (
        <div className="completed-box">
          <span className="icon is-large">
            <i className="fa fa-check has-text-success" />
          </span>
          <h3>Completed!</h3>
          <a
            className="relinquish-task has-text-info is-size-9"
            onClick={() => {
              this.props.unfinishTask(this.props.task);
            }}
          >
            MARK AS INCOMPLETE
          </a>
        </div>
      );
    }
    return (
      <div>
        <a
          className="button is-fullwidth is-info is-size-9 button-continue"
          onClick={() => {
            this.props.finishTask(this.props.task);
          }}
        >
          SIGN OFF
        </a>
        <a
          className="relinquish-task has-text-danger is-size-9"
          onClick={() => {
            this.props.updateTask({
              ...this.props.task,
              finished: this.props.task.finished ? 'true' : 'false',
              id_user_owner: null,
              date_started: null,
              num_col: 0
            });

            this.props.setStep(0);
          }}
        >
          RELINQUISH TASK
        </a>
      </div>
    );
  }

  render() {
    if (!this.props.task.id_user_owner) return <h1>Loading...</h1>;

    const dueDate = moment(this.props.task.date_started).add(
      this.props.task.DaysAllowed,
      'days'
    );

    return (
      <div className="info-panel has-text-grey">
        <ul>
          <li>
            <article className="media">
              <figure className="media-left">
                <span className="icon is-medium">
                  <i className="fa fa-calendar" />
                </span>
              </figure>
              <div className="media-content">
                <div className="content">
                  <p>
                    <small>Due Date:</small>
                    <strong className="has-text-grey">
                      {dueDate.format('MM-DD-YYYY')}
                    </strong>
                  </p>
                </div>
              </div>
            </article>
          </li>
          <li>
            <article className="media">
              <figure className="media-left">
                <span className="icon is-medium">
                  <i className="fa fa-clock-o" />
                </span>
              </figure>
              <div className="media-content">
                <div className="content">
                  <p>
                    <small>Workload:</small>
                    <strong className="has-text-grey">
                      {this.props.task.hours} Hrs
                    </strong>
                  </p>
                </div>
              </div>
            </article>
          </li>
          <li>
            <article className="media">
              <figure className="media-left">
                <span className="icon is-medium">
                  <i className="fa fa-user" />
                </span>
              </figure>
              <div className="media-content">
                <div className="content">
                  <p>
                    <small>User:</small>
                    <strong className="has-text-grey">
                      {this.props.task.OwnerName}
                    </strong>
                  </p>
                </div>
              </div>
            </article>
          </li>
        </ul>

        <hr />

        {this.renderBottom(this.props.task.finished)}
      </div>
    );
  }
}

export default connect(null, actions)(InfoPanel);
