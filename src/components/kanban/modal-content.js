import React, { Component } from "react";
import moment from "moment";

class ModalContent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      tab: "description"
    };
  }

  renderInfoBoxes() {
    const currentDate = moment();
    const dueDate = moment(this.props.task.date_started).add(
      this.props.task.DaysAllowed,
      "days"
    );
    const daysLeft = dueDate.diff(currentDate, "days");

    if (this.props.task.id_user_owner) {
      return (
        <div className="level-item">
          <div
            className={`level-item info-box has-text-${
              daysLeft < 0 ? "danger" : "success"
            }`}
          >
            <p>DAYS LEFT</p>
            <h4 className="is-size-4">{daysLeft < 0 ? 0 : daysLeft}</h4>
          </div>
          <div className="level-item info-box has-text-info">
            <p>POINTS</p>
            <h4 className="is-size-4">{this.props.task.points}</h4>
          </div>
        </div>
      );
    }
  }

  renderTaskStarted() {
    if (this.props.task.id_user_owner) {
      return (
        <p className="is-size-9 has-text-grey-light">
          <span className="has-text-grey">TASK STARTED:</span>{" "}
          {moment(this.props.task.date_started).format("MM-DD-YYYY")}
        </p>
      );
    }
  }

  renderContent() {
    switch (this.state.tab) {
      case "collaborators":
        return (
          <div className="task-modal-description">
            {this.props.task.users.map((user, index) => {
              return <h1 key={index}>{user.username}</h1>;
            })}
          </div>
        );

      case "description":
      default:
        return (
          <div className="task-modal-description">
            <p>{this.props.task.description}</p>
            <a className="button is-info is-size-9 button-continue">
              REQUEST CLARIFICATION
            </a>
          </div>
        );
    }
  }

  // TODO this needs to be abstracted into general docs I think, but it might be a backend revision
  renderSlackUrl(task) {
    if (task.url_slack) {
      return (
        <li>
          <a href={task.url_slack}>{task.url_slack}</a>
        </li>
      );
    }
  }

  renderGithubUrl(task) {
    if (task.url_github) {
      return (
        <li>
          <a href={task.url_github}>{task.url_github}</a>
        </li>
      );
    }
  }

  renderDocuments(task) {
    if (task.documents) {
      return task.documents.map((doc, index) => {
        return (
          <li key={index}>
            {doc.name} <a href={doc.url}>{doc.url}</a>
          </li>
        );
      });
    }
  }

  renderSkills(task) {
    if (task.skills) {
      return task.skills.map((skill, index) => {
        return (
          <li
            key={index}
            className="level-item button is-small is-outlined is-info"
          >
            {skill.name}
          </li>
        );
      });
    }
  }

  render() {
    return (
      <div className="ds-task-modal-content column">
        <nav className="level">
          <div className="level-left">
            <div className="level-item">
              <p className="title is-4">{this.props.task.TaskName}</p>
              <div className="tags">
                <span
                  className={`ds-tag tag ${
                    !this.props.task.finished ? "" : "is-danger"
                  }`}
                >
                  {!this.props.task.finished ? "OPEN" : "CLOSED"}
                </span>
                {this.renderTaskStarted()}
              </div>
            </div>
          </div>
          <div className="level-right">
            {this.renderInfoBoxes()}
            <a
              className="level-item"
              style={{ marginLeft: "1rem" }}
              onClick={() => {
                this.props.editTask(this.props.task);
              }}
            >
              <span className="icon has-text-grey">
                <i className="fa fa-edit" />
              </span>
            </a>
            <a
              className="level-item"
              onClick={() => {
                this.props.deleteTask(this.props.task);
                this.props.notify({ type: "danger", text: "Task Deleted!" });
                this.props.setActiveTask(null);
              }}
            >
              <span className="icon has-text-danger">
                <i className="fa fa-trash-o" />
              </span>
            </a>
          </div>
        </nav>

        <div className="ds-task-nav breadcrumb">
          <ul className="is-size-9">
            <li className={this.state.tab === "description" ? "is-active" : ""}>
              <a
                onClick={() => {
                  this.setState({ tab: "description" });
                }}
              >
                DESCRIPTION
              </a>
            </li>
            <li>
              <a>SUBTASKS</a>
            </li>
            <li
              className={this.state.tab === "collaborators" ? "is-active" : ""}
            >
              <a
                onClick={() => {
                  this.setState({ tab: "collaborators" });
                }}
              >
                COLLABORATORS
              </a>
            </li>
          </ul>
        </div>

        {this.renderContent()}

        <hr className="task-modal-hr" />

        <div className="level is-shadowless is-paddingless">
          <div className="level-left">
            <div className="level-item">
              <ul className="is-size-9 is-weight-bold">
                <strong className="is-size-7">Documents</strong>
                {this.renderDocuments(this.props.task)}
              </ul>
            </div>
          </div>
          <div className="level-right">
            <div className="level-item">
              <ul className="is-size-9 is-weight-bold">
                <strong className="is-size-7">Slack discussion</strong>
                {this.renderSlackUrl(this.props.task)}
                <strong className="is-size-7">Github discussion</strong>
                {this.renderGithubUrl(this.props.task)}
              </ul>
            </div>
          </div>
        </div>

        <div>
          <ul className="level-left">{this.renderSkills(this.props.task)}</ul>
        </div>
      </div>
    );
  }
}

export default ModalContent;
