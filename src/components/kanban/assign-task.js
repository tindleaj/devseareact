import React, { Component } from "react";

class AssignTask extends Component {
  render() {
    function renderUsers(usersList) {
      // Better to use an arbitrary value for 'key' instead of a value passed from the state or props
      // so that duplicate keys are never an issue.
      return usersList.map((user, i) => {
        return (
          <option key={i} value={user.id}>{`${user.firstname} ${user.lastname ||
            ""} - ${user.username}`}</option>
        );
      });
    }

    if (this.props.me.id_role === 2) {
      return (
        <div>
          <div className="field">
            <label htmlFor="continue" className="label is-size-4">
              Who would you like to assign this task to?
            </label>
            <div className="control">
              <div className="select is-fullwidth">
                <select ref="userId">
                  {renderUsers(this.props.usersList)}
                </select>
              </div>
            </div>
            <br />
            <div className="control">
              <a
                onClick={() =>
                  this.props.nextStep({
                    id_user_owner: parseInt(this.refs.userId.value, 10)
                  })
                }
                className="button is-fullwidth is-info is-size-9 button-continue"
              >
                SELECT
              </a>
            </div>
          </div>
        </div>
      );
    }

    return (
      <div>
        <div className="field">
          <label htmlFor="continue" className="label is-size-4">
            Do you want to assign yourself as owner for this task?
          </label>
          <div className="control">
            <a
              onClick={() =>
                this.props.nextStep({ id_user_owner: this.props.me.id })
              }
              className="button is-fullwidth is-info is-size-9 button-continue"
            >
              YES
            </a>
          </div>
        </div>
      </div>
    );
  }
}

export default AssignTask;
