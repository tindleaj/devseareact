import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions/index';
import ProfileCard from './profile-card';

class ProfileContainer extends Component {
  state = {
    isEditing: false,
    user: this.props.me,
  };
  handleEditingChange = value => {
    this.setState({ isEditing: value });
  };
  handleCancel = () => {
    this.setState({ isEditing: false, user: this.props.me });
  };
  handleChange = name => event => {
    this.setState({
      user: { ...this.state.user, [name]: event.target.value },
    });
  };
  uploadFile = file => {
    this.props.updateProfilePicture(this.props.me.id, file);
  };
  handleFormSubmit = e => {
    if (e) {
      e.preventDefault();
    }
    this.setState({ isEditing: false }, () =>
      this.props.updateMe(this.state.user)
    );
  };
  render() {
    return (
      <section className="section ds-content">
        <ProfileCard
          user={this.state.user}
          isEditing={this.state.isEditing}
          onFormSubmit={this.handleFormSubmit}
          onChange={this.handleChange}
          onCancel={this.handleCancel}
          onEditingChange={this.handleEditingChange}
          uploadFile={this.uploadFile}
        />
      </section>
    );
  }
}

function mapStateToProps(state) {
  return {
    me: state.users.me,
  };
}

export default connect(mapStateToProps, actions)(ProfileContainer);
