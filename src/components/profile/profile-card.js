import React from 'react';
import PropTypes from 'prop-types';

const ProfileCard = ({
  user,
  onCancel,
  isEditing,
  onFormSubmit,
  onEditingChange,
  onChange,
  uploadFile,
}) => {
  const renderActionButtons = () => {
    return isEditing ? (
      <div>
        <a className="button is-red is-pulled-right" onClick={() => onCancel()}>
          Cancel
        </a>
        <button
          className="button is-secondary-blue is-pulled-right"
          type="submit"
        >
          Save
        </button>
      </div>
    ) : (
      <a
        className="button is-primary is-outlined is-pulled-right"
        onClick={() => onEditingChange(true)}
      >
        Edit
      </a>
    );
  };
  let fileName = '...';
  const upload = event => {
    fileName = event.target.value;
    const file = event.target.files[0];
    uploadFile(file);
    // var reader = new FileReader();
    // reader.readAsDataURL(file);
    // reader.onload = function(e) {
    //   debugger
    //   uploadFile(e.target.result);
    // };
  };

  const renderField = (label, name, value, required = true) => {
    return (
      <div className="field">
        <div className="field-label is-pulled-left">
          <label className="label">{label}</label>
        </div>
        <div className="control is-expanded">
          <input
            name={name}
            type="text"
            className="input"
            defaultValue={value}
            onChange={onChange(name)}
            required={required}
          />
        </div>
      </div>
    );
  };

  return (
    <form className="card profile-card" onSubmit={onFormSubmit}>
      <div className="card-header profile-card__header">
        <img
          className="profile-card__image"
          src={
            user.picture_url ||
            'https://i.pinimg.com/236x/8d/a1/39/8da1395cce1ee57ebab406b72af11031--mature-old-old-person.jpg'
          }
          alt="user profile"
        />
      </div>
      <div className="card-content profile-card__content">
        {isEditing ? (
          <div>
            {renderField('First Name', 'firstname', user.firstname)}
            {renderField('Last Name', 'lastname', user.lastname)}
          </div>
        ) : (
          <div className="is-12">
            <span className="title">{`${user.firstname} ${
              user.lastname
            }`}</span>
          </div>
        )}
        {isEditing ? null : (
          <div className="is-12">
            <span className="subtitle is-12">{user.email}</span>
          </div>
        )}
        {isEditing ? null : (
          <div className="is-12">
            <span className="location is-12">{user.username}</span>
          </div>
        )}
        {isEditing ? (
          renderField('Bio', 'bio', user.bio, false)
        ) : (
          <div className="profile-card__bio">{user.bio}</div>
        )}
        {/* {isEditing ? (
          <div>
            <div className="control is-expanded">
              <div className="file is-info has-name">
                <label className="file-label">
                  <input
                    className="file-input"
                    type="file"
                    name="resume"
                    accept=".jpg, .jpeg, .png"
                    onChange={upload}
                  />
                  <span className="file-cta">
                    <span className="file-icon">
                      <i className="fa fa-upload" />
                    </span>
                    <span className="file-label">Profile Picture</span>
                  </span>
                  <span className="file-name" style={{ minWidth: '150px' }}>
                    {fileName}
                  </span>
                </label>
              </div>
            </div>
          </div>
        ) : null} */}
        <div className="profile-card__footer is-12">
          {renderActionButtons()}
        </div>
      </div>
    </form>
  );
};

ProfileCard.propTypes = {
  user: PropTypes.object.isRequired,
  onCancel: PropTypes.func,
  isEditing: PropTypes.bool,
  onFormSubmit: PropTypes.func,
  onEditingChange: PropTypes.func,
  onChange: PropTypes.func,
};

ProfileCard.defaultProps = {
  onCancel: () => {},
  isEditing: false,
  onFormSubmit: () => {},
  onEditingChange: () => {},
  onChange: () => {},
};

export default ProfileCard;
