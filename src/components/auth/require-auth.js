import React, { Component } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import Loader from "../loader";

import * as actions from "../../actions";

// Higher-Order Component for authentication
export default function(ComposedComponent) {
  class Authentication extends Component {
    componentWillMount() {
      if (this.props.authenticated) {
        this.props.fetchAll();
      } else {
        this.props.history.push("/auth/login");
      }
    }

    render() {
      if (this.props.projectsList && this.props.projectsList.length === 0)
        return <Redirect to="/discover/browse" />;

      if (!this.props.me) return <Loader />;

      return <ComposedComponent {...this.props} />;
    }
  }

  function mapStateToProps(state) {
    return {
      authenticated: state.auth.authenticated,
      me: state.users.me,
      projectsList: state.projects.projectsList
    };
  }

  return connect(mapStateToProps, actions)(Authentication);
}
