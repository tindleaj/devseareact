import React, { Component } from 'react';
import { connect } from 'react-redux';

class ConfirmUser extends Component {
  render() {
    return (
      <div className="column is-one-third is-offset-one-third ds-content-unauthed">
        <div className="content has-text-centered">
          <h2 className="title is-2">Thanks for signing up!</h2>
          <p>
            You need to confirm your email address before you can continue registration. We have
            sent an email to:
          </p>
          <div className="content notification is-light is-large">
            <p>
              <strong>
                {this.props.confirmUserEmail}
              </strong>
            </p>
          </div>

          <button className="button is-info is-fullwidth is-large">Resend Email</button>
          <br />
          <p>Please make sure to check your spam folder!</p>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return { confirmUserEmail: state.auth.confirmUserEmail };
}

export default connect(mapStateToProps)(ConfirmUser);
