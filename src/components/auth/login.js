import React, { Component } from 'react';
import { reduxForm, Field } from 'redux-form';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Link } from 'react-router-dom';

import * as actions from '../../actions';

import background from '../../img/welcome-background.jpg';

class Login extends Component {
  handleFormSubmit({ username, password }) {
    // login user
    this.props.loginUser(username, password, this.props.history);
  }

  renderAlert() {
    if (this.props.errorMessage) {
      return (
        <div className="message is-danger">
          <div className="message-body">Oops! {this.props.errorMessage}</div>
        </div>
      );
    }
  }

  render() {
    const { handleSubmit, fields: { username, password } } = this.props;

    return (
      <div className="columns ds-content" style={{ backgroundImage: `url(/${background})` }}>
        <div className="column is-4 is-offset-2 ds-login-form">
          <figure className="content has-text-centered">
            <h2 className="title is-2 has-text-white">Your journey starts here</h2>
          </figure>
          <br />

          <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
            <div className="field">
              <p className="control">
                <Field
                  component="input"
                  name="username"
                  className="input is-medium"
                  type="text"
                  placeholder="Username"
                  {...username}
                />
              </p>
            </div>

            <div className="field">
              <p className="control">
                <Field
                  component="input"
                  name="password"
                  className="input is-medium"
                  type="password"
                  placeholder="Password"
                  {...password}
                />
              </p>
              <p className="help">
                <Link className="has-text-white" to="/auth/forgot">
                  Forgot password?
                </Link>
              </p>
            </div>

            {this.renderAlert()}
            <button action="submit" className="button is-info ds-primary-button">
              Login
            </button>
          </form>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return { errorMessage: state.auth.error };
}

const loginForm = reduxForm({
  form: 'login',
  fields: ['username', 'password'],
})(Login);

// withRouter works like connect()() or at least has the same signature.
// react-router v4 advises that you wrap connect with withRouter like this
export default withRouter(connect(mapStateToProps, actions)(loginForm));
