import React, { Component } from "react";
import { reduxForm, Field } from "redux-form";
import { connect } from "react-redux";
import { withRouter } from "react-router";

import * as actions from "../../actions";

import background from "../../img/welcome-background.jpg";

class Signup extends Component {
  handleFormSubmit({ firstname, lastname, username, email, password }) {
    // Get token if it's there
    const token = this.props.match.params.token || null;

    // signup user
    this.props.signupUser(
      firstname,
      lastname,
      username,
      email,
      password,
      token,
      this.props.history
    );
  }

  renderEmailInfo() {
    return (
      <div className="notification is-info">
        <p>
          Please use the same email address you were invited with. You can
          change or add email addresses once your account has been created.
        </p>
      </div>
    );
  }

  render() {
    const {
      handleSubmit,
      fields: { firstname, lastname, username, email, password }
    } = this.props;

    return (
      <div
        className="columns ds-content"
        style={{ backgroundImage: `url(/${background})` }}
      >
        <div className="column is-4 is-offset-2 ds-login-form">
          <figure className="content has-text-centered">
            <h2 className="title is-2 has-text-white has-text-weight-bold">
              Join the future of work
            </h2>
            {this.props.match.params.token ? this.renderEmailInfo() : null}
          </figure>
          <br />

          <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
            <div className="field">
              <p className="control">
                <Field
                  component="input"
                  name="firstname"
                  className="input is-medium"
                  type="text"
                  placeholder="First Name"
                  {...firstname}
                />
              </p>
            </div>

            <div className="field">
              <p className="control">
                <Field
                  component="input"
                  name="lastname"
                  className="input is-medium"
                  type="text"
                  placeholder="Last Name"
                  {...lastname}
                />
              </p>
            </div>

            <div className="field">
              <p className="control">
                <Field
                  component="input"
                  name="username"
                  className="input is-medium"
                  type="text"
                  placeholder="Username"
                  {...username}
                />
              </p>
            </div>

            <div className="field">
              <p className="control">
                <Field
                  component="input"
                  name="email"
                  className="input is-medium"
                  type="text"
                  placeholder="Email"
                  {...email}
                />
              </p>
            </div>

            <div className="field">
              <p className="control">
                <Field
                  component="input"
                  name="password"
                  className="input is-medium"
                  type="password"
                  placeholder="Password"
                  {...password}
                />
              </p>
            </div>

            <button
              action="submit"
              className="button is-info ds-primary-button"
            >
              Signup
            </button>
          </form>
        </div>
      </div>
    );
  }
}

const signupForm = reduxForm({
  form: "signup",
  fields: ["firstname", "lastname", "username", "email", "password"]
})(Signup);

export default withRouter(connect(null, actions)(signupForm));
