import React from 'react';

import background from '../../img/welcome-background.jpg';

const ForgotPassword = () => (
  <div className="columns ds-content" style={{ backgroundImage: `url(/${background})` }}>
    <div className="column is-one-third is-offset-one-third ds-login-form">
      <div className="content has-text-centered has-text-white">
        <h2 className="title is-2 has-text-white">Forgot your password?</h2>
        <p>
          No problem. Just enter the email associated with your account below, and we'll send you a
          link to reset your password.
        </p>
        <div className="field">
          <input className="input" />
        </div>

        <button className="button is-info is-fullwidth is-large">Send Email</button>
        <br />
        <p>Please make sure to check your spam folder!</p>
      </div>
    </div>
  </div>
);

export default ForgotPassword;
