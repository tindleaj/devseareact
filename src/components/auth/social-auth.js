import React from 'react';

const SocialAuth = () =>
  <div>
    <div className="block">
      <button className="button is-primary is-fullwidth is-large">Sign up with LinkedIn</button>
    </div>
    <div className="block">
      <button className="button is-info is-fullwidth is-large">Sign up with Facebook</button>
    </div>
  </div>;

export default SocialAuth;
