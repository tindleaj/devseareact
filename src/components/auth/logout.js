import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import * as actions from '../../actions';

import background from '../../img/welcome-background.jpg';

class Logout extends Component {
  componentDidMount() {
    this.props.logoutUser();
  }

  render() {
    return (
      <div className="columns ds-content" style={{ backgroundImage: `url(/${background})` }}>
        <div className="column is-one-third is-offset-one-third ds-login-form">
          <div className="content has-text-centered has-text-white">
            <h2 className="title is-2 has-text-centered has-text-white">
              You have been logged out.
            </h2>
            <p>Come back soon! If you did not mean to log out, log back in below.</p>

            <Link to="/auth/login" className="button is-info ds-primary-button">
              Login
            </Link>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    authenticated: state.auth.authenticated,
  };
}

export default connect(mapStateToProps, actions)(Logout);
