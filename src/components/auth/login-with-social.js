import React, { Component } from 'react';
import { reduxForm, Field } from 'redux-form';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import * as actions from '../../actions';

import background from '../../img/welcome-background.jpg';

class Signup extends Component {
  handleFormSubmit({ firstname, lastname, username, email, password }) {
    // signup user
    this.props.signupUser(firstname, lastname, username, email, password, this.props.history);
  }

  render() {
    const { handleSubmit, fields: { firstname, lastname, username, email, password } } = this.props;

    return (
      <div className="columns ds-content" style={{ backgroundImage: `url(/${background})` }}>
        <div className="column is-4 is-offset-2 ds-login-form">
          <figure className="content has-text-centered">
            <h2 className="title is-2 has-text-white">Join the future of work</h2>
          </figure>
          <br />

          <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
            <div className="field">
              <a className="button is-medium is-fullwidth is-linkedin-blue">
                <span className="icon">
                  <i className="fa fa-linkedin" />
                </span>
                <span>Sign up with LinkedIn</span>
              </a>
            </div>

            <div className="field">
              <a className="button is-medium is-fullwidth is-facebook-blue">
                <span className="icon">
                  <i className="fa fa-facebook-official" />
                </span>
                <span>Sign up with Facebook</span>
              </a>
            </div>

            <div className="block">
              <div className="columns is-mobile">
                <div className="column">
                  <hr className="" />
                </div>
                <div className="column">
                  <div className="is-vertically-aligned">
                    <h3 className="title is-5 has-text-centered has-text-white">OR</h3>
                  </div>
                </div>
                <div className="column">
                  <hr />
                </div>
              </div>
            </div>

            <div className="field">
              <p className="control">
                <Field
                  component="input"
                  name="firstname"
                  className="input is-medium"
                  type="text"
                  placeholder="First Name"
                  {...firstname}
                />
              </p>
            </div>

            <div className="field">
              <p className="control">
                <Field
                  component="input"
                  name="lastname"
                  className="input is-medium"
                  type="text"
                  placeholder="Last Name"
                  {...lastname}
                />
              </p>
            </div>

            <div className="field">
              <p className="control">
                <Field
                  component="input"
                  name="username"
                  className="input is-medium"
                  type="text"
                  placeholder="Username"
                  {...username}
                />
              </p>
            </div>

            <div className="field">
              <p className="control">
                <Field
                  component="input"
                  name="email"
                  className="input is-medium"
                  type="text"
                  placeholder="Email"
                  {...email}
                />
              </p>
            </div>

            <div className="field">
              <p className="control">
                <Field
                  component="input"
                  name="password"
                  className="input is-medium"
                  type="password"
                  placeholder="Password"
                  {...password}
                />
              </p>
            </div>

            <button action="submit" className="button is-info ds-primary-button">
              Signup
            </button>
          </form>
        </div>
      </div>
    );
  }
}

const signupForm = reduxForm({
  form: 'signup',
  fields: ['firstname', 'lastname', 'username', 'email', 'password'],
})(Signup);

export default withRouter(connect(null, actions)(signupForm));
