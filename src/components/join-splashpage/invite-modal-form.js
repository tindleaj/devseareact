import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';

import * as actions from '../../actions';

class InviteModalForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
    };
  }

  handleFormSubmit(wait_list_user) {
    this.setState({ isLoading: true });

    this.props.addWaitUser(wait_list_user).then(res => {
      this.props.notify({ type: 'success', text: 'Invite sent successfully!' });
      this.props.onClick();
    });
  }

  render() {
    const {
      handleSubmit,
      fields: { firstname, lastname, email, registration_type, role, telephone, message },
    } = this.props;

    return (
      <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
        <div className="field is-horizontal">
          <div className="field-label is-normal">
            <label htmlFor="role" className="label">
              I want to
            </label>
          </div>
          <div className="field-body">
            <div className="field">
              <div className="control">
                <div className="select is-fullwidth">
                  <Field name="role" component="select" {...role}>
                    <option value="developer">Contribute to digital projects</option>
                    <option value="startup">Create digital projects</option>
                    <option value="both">Both</option>
                  </Field>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="field is-horizontal">
          <div className="field-label is-normal">
            <label htmlFor="registration_type" className="label">
              I am a
            </label>
          </div>
          <div className="field-body">
            <div className="field">
              <div className="control">
                <div className="select is-fullwidth">
                  <Field name="registration_type" component="select" {...registration_type}>
                    <option value="1">Developer</option>
                    <option value="2">Startup Founder</option>
                    <option value="3">Investor</option>
                  </Field>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="field is-horizontal">
          <div className="field-label is-grouped">
            <label htmlFor="firstname" className="label">
              Name
            </label>
          </div>
          <div className="field-body">
            <div className="field is-grouped">
              <p className="control is-expanded">
                <Field
                  className="input"
                  name="firstname"
                  component="input"
                  type="text"
                  placeholder="First"
                  {...firstname}
                />
              </p>
              <p className="control is-expanded">
                <Field
                  className="input"
                  name="lastname"
                  component="input"
                  type="text"
                  placeholder="Last"
                  {...lastname}
                />
              </p>
            </div>
          </div>
        </div>

        <div className="field is-horizontal">
          <div className="field-label is-normal">
            <label htmlFor="email" className="label">
              Email
            </label>
          </div>
          <div className="field-body">
            <div className="field">
              <p className="control is-expanded">
                <Field
                  className="input"
                  name="email"
                  component="input"
                  type="text"
                  placeholder="Email"
                  {...email}
                />
              </p>
            </div>
          </div>
        </div>

        <div className="field is-horizontal">
          <div className="field-label is-normal">
            <label htmlFor="message" className="label">
              Message
            </label>
          </div>
          <div className="field-body">
            <div className="field">
              <p className="control is-expanded">
                <Field
                  className="textarea"
                  name="message"
                  component="textarea"
                  type="text"
                  placeholder="Message"
                  {...message}
                />
              </p>
            </div>
          </div>
        </div>

        <div className="field is-horizontal">
          <div className="field-label is-normal">
            <label htmlFor="telephone" className="label">
              Phone (Optional)
            </label>
          </div>
          <div className="field-body">
            <div className="field">
              <p className="control is-expanded">
                <Field
                  className="input"
                  name="telephone"
                  component="input"
                  type="text"
                  placeholder="Phone"
                  {...telephone}
                />
              </p>
            </div>
          </div>
        </div>

        <br />

        <div className="field is-horizontal">
          <div className="field-label is-normal" />
          <div className="field-body">
            <div className="field">
              <button
                action="submit"
                className={`button ds-primary-button is-info ${this.state.isLoading
                  ? 'is-loading'
                  : ''}`}
              >
                Go!
              </button>
            </div>
          </div>
        </div>

        <br />
      </form>
    );
  }
}

function mapStateToProps(state) {
  return {
    initialValues: {
      registration_type: '1',
      role: 'developer',
    },
  };
}

const inviteForm = reduxForm({
  form: 'invite',
  fields: ['firstname', 'lastname', 'email', 'registration_type', 'role', 'telephone', 'message'],
})(InviteModalForm);

export default connect(mapStateToProps, actions)(inviteForm);
