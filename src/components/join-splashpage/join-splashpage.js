import React, { Component } from 'react';

import InviteModal from './invite-modal';

import logoIcon from '../../img/logo-icon.png';
import splashBackground from '../../img/splash-background.jpeg';

class JoinSplashpage extends Component {
	constructor(props) {
		super(props);

		this.state = {
			isInviting: false,
		};
	}

	renderModal(isInviting) {
		if (isInviting) {
			return (
				<InviteModal
					onClick={() => {
						this.setState({ isInviting: false });
					}}
				/>
			);
		}

		return null;
	}

	render() {
		return (
			<section className="is-paddingless">
				<section
					className="hero is-dark is-large ds-join-splash"
					style={{ backgroundImage: `url(${splashBackground})` }}
				>
					{this.renderModal(this.state.isInviting)}
					<div className="hero-body">
						<div className="container">
							<nav className="level is-shadowless is-paddingless is-mobile">
								<div className="level-left">
									<figure className="level-item image is-96x96">
										<img alt="logo-icon" src={logoIcon} />
									</figure>

									<h1 className="level-item title is-1">DEVSEA</h1>
								</div>
							</nav>

							<br />
							<br />

							<h2 className="title">Empowering Digital Creatives</h2>
							<h3 className="subtitle">
								Build your dream project | Join a dream team
							</h3>

							<div className="field is-grouped">
								<p className="control">
									<a
										className="button is-info"
										onClick={() => {
											this.setState({ isInviting: true });
										}}
									>
										Request an invite
									</a>
								</p>
								<p className="control">
									<a
										className="button is-light is-outlined"
										href="mailto:contact@devsea.io?Subject=Devsea%20Invite"
									>
										Contact
									</a>
								</p>
							</div>

							<nav className="level is-shadowless is-paddingless is-mobile">
								<div className="level-left">
									<a className="level-item" href="https://twitter.com/Devseaio">
										<span className="icon is-medium">
											<i className="fa fa-twitter" />
										</span>
									</a>
									<a
										className="level-item"
										href="https://www.facebook.com/Devsea-1472924852793377/"
									>
										<span className="icon is-medium">
											<i className="fa fa-facebook" />
										</span>
									</a>
									<a className="level-item" href="https://medium.com/devsea">
										<span className="icon is-medium">
											<i className="fa fa-medium" />
										</span>
									</a>
								</div>
							</nav>
						</div>
					</div>
				</section>
			</section>
		);
	}
}

export default JoinSplashpage;
