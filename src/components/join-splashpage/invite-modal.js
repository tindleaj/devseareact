import React, { Component } from 'react';

import InviteModalForm from './invite-modal-form';

class InviteModal extends Component {
  render() {
    return (
      <div className="modal is-active">
        <div className="modal-background" onClick={() => this.props.onClick()} />
        <div className="modal-card">
          <header className="modal-card-head">
            <p className="modal-card-title">Tell us about yourself</p>
            <button className="delete" aria-label="close" onClick={() => this.props.onClick()} />
          </header>
          <section className="modal-card-body">
            <InviteModalForm onClick={() => this.props.onClick()} />
          </section>
        </div>
      </div>
    );
  }
}

export default InviteModal;
