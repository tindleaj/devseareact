import {
  FETCH_TOPICS,
  SET_ACTIVE_PROJECT,
  UNAUTH_USER,
  SET_ACTIVE_TOPIC,
  ADD_TOPIC,
  UPDATE_TOPIC,
  DELETE_TOPIC,
  ADD_TASK,
  UPDATE_TASK,
  DELETE_TASK,
} from '../actions/types';

export default function(state = {}, action) {
  switch (action.type) {
    case FETCH_TOPICS:
      return {
        ...state,
        topicsList: action.payload.data,
      };

    case SET_ACTIVE_TOPIC:
      return {
        ...state,
        activeTopic: state.topicsList.find(topic => {
          return topic.id === parseInt(action.payload, 10);
        }),
      };

    case SET_ACTIVE_PROJECT:
      return {};

    case UNAUTH_USER:
      return {};

    case ADD_TOPIC:
      return {
        ...state,
        topicsList: [...state.topicsList, { ...action.payload.data, totalPoints: 0 }],
      };

    case UPDATE_TOPIC:
      return {
        ...state,
        topicsList: state.topicsList.map(
          topic => (topic.id === action.payload.data.id ? action.payload.data : topic)
        ),
      };

      case DELETE_TOPIC:
      return {
        ...state,
        topicsList: state.topicsList.filter(
          topic => topic.id !== action.payload.data.id
        ),
      };

    case ADD_TASK:
    case UPDATE_TASK:
    case DELETE_TASK:
      return {
        ...state,
        topicsList: state.topicsList.map(topic => {
          if (topic.id === action.payload.data.id_project_section_topic)
            topic.totalPoints += action.points || 0;
          return topic;
        }),
      };

    default:
      return state;
  }
}
