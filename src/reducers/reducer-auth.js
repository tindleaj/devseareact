import {
  AUTH_USER,
  UNAUTH_USER,
  CONFIRM_USER,
  SHOW_SIDEBAR,
  HIDE_SIDEBAR
} from "../actions/types";

export default function(state = {}, action) {
  switch (action.type) {
    case AUTH_USER:
      return { ...state, authenticated: true };
    case UNAUTH_USER:
      return { ...state, authenticated: false };
    case CONFIRM_USER:
      return { ...state, confirmUserEmail: action.payload };
    case HIDE_SIDEBAR:
      return { ...state, hideSidebar: true };
    case SHOW_SIDEBAR:
      return { ...state, hideSidebar: false };
    default:
      return state;
  }
}
