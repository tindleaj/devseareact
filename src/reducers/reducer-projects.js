import {
  FETCH_PROJECTS,
  SET_ACTIVE_PROJECT,
  UNAUTH_USER,
  FETCH_ACTIVE_PROJECT_LEADERBOARD,
  ADD_PROJECT,
  UPDATE_PROJECT,
} from '../actions/types';

// This reducer needs to fit the format the other do,
// and contain projectsList and activeProject nested inside the parent property of projects
// This will break the current implementation of the task management overview
export default function(state = {}, action) {
  switch (action.type) {
    case FETCH_PROJECTS:
      return {
        ...state,
        projectsList: action.payload,
      };

    case SET_ACTIVE_PROJECT:
      return {
        ...state,
        activeProject: action.payload,
      };

    case ADD_PROJECT:
      return {
        ...state,
        projectsList: [...state.projectsList, action.payload.data],
      };

    case UPDATE_PROJECT:
      return {
        ...state,
        projectsList: state.projectsList.map(
          project =>
            project.id === action.payload.data.id
              ? action.payload.data
              : project,
        ),
      };

    case UNAUTH_USER:
      return {};

    case FETCH_ACTIVE_PROJECT_LEADERBOARD:
      return {
        ...state,
        leaderboard: action.payload,
      };

    default:
      return state;
  }
}
