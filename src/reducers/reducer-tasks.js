import {
  FETCH_TASKS,
  CLEAR_TASKS,
  SET_ACTIVE_PROJECT,
  SET_ACTIVE_TASK,
  UPDATE_TASK,
  ADD_TASK,
  UNAUTH_USER,
  DELETE_TASK,
  ADD_TASK_USER
} from '../actions/types';

export default function(state = {}, action) {
  switch (action.type) {
    case FETCH_TASKS:
      return {
        ...state,
        tasksList: action.payload.data
      };

    case CLEAR_TASKS:
      return {
        ...state,
        tasksList: null
      };

    case SET_ACTIVE_PROJECT:
      return {};

    case SET_ACTIVE_TASK:
      return {
        ...state,
        activeTask: action.payload
      };

    case ADD_TASK:
      return {
        ...state,
        tasksList: [...state.tasksList, action.payload.data]
      };

    case ADD_TASK_USER:
      return {
        ...state,
        tasksList: state.tasksList.map(task => {
          if (task.id === action.taskId)
            return { ...task, users: [...task.users, action.payload.data] };

          return task;
        }),
        activeTask:
          state.activeTask && state.activeTask.id === action.taskId
            ? {
                ...state.activeTask,
                users: [...state.activeTask.users, action.payload.data]
              }
            : state.activeTask
      };

    case UPDATE_TASK:
      return {
        ...state,
        tasksList: state.tasksList.map(
          task =>
            task.id === action.payload.data.id
              ? { ...task, ...action.payload.data }
              : task
        ),
        activeTask: action.activeTask
          ? { ...state.activeTask, ...action.activeTask }
          : null
      };

    case DELETE_TASK:
      return {
        ...state,
        tasksList: state.tasksList.filter(
          task => task.id !== action.payload.data.id
        )
      };

    case UNAUTH_USER:
      return {};

    default:
      return state;
  }
}
