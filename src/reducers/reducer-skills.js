import { FETCH_SKILLS } from '../actions/types';

export default function(state = {}, action) {
  switch (action.type) {
    case FETCH_SKILLS:
      return {
        ...state,
        skillsList: action.payload.data,
      };

    default:
      return state;
  }
}
