import { FETCH_SECTIONS, UPDATE_SECTION, ADD_SECTION, DELETE_SECTION } from '../actions/types';

export default function(state = {}, action) {
	switch (action.type) {
		case FETCH_SECTIONS:
			return {
				...state,
				sectionsList: action.payload.data.sort((a, b) => {
					return a.id - b.id;
				}),
			};

		case ADD_SECTION:
			return {
				...state,
				sectionsList: [...state.sectionsList, action.payload.data],
			};

		case UPDATE_SECTION:
			return {
				...state,
				sectionsList: state.sectionsList.map(
					section =>
						section.id === action.payload.data.id ? action.payload.data : section
				),
			};
		case DELETE_SECTION:
		return {
			...state,
			sectionsList: state.sectionsList.filter(
				section =>
					section.id !== action.payload.data.id
			),
		};

		default:
			return state;
	}
}
