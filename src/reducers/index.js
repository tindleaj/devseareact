import { combineReducers } from "redux";
import { reducer as formReducer } from "redux-form";
import authReducer from "./reducer-auth";
import projectsReducer from "./reducer-projects";
import sectionsReducer from "./reducer-sections";
import topicsReducer from "./reducer-topics";
import tasksReducer from "./reducer-tasks";
import skillsReducer from "./reducer-skills";
import usersReducer from "./reducer-users";
import errorsReducer from "./reducer-errors";

const rootReducer = combineReducers({
  form: formReducer,
  auth: authReducer,
  projects: projectsReducer,
  sections: sectionsReducer,
  topics: topicsReducer,
  tasks: tasksReducer,
  skills: skillsReducer,
  users: usersReducer,
  errors: errorsReducer
});

export default rootReducer;
