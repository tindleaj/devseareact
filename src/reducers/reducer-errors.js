import { ERRORS } from "../actions/types";

export default function(state = {}, error) {
  switch (error.type) {
    case ERRORS.SIGNUP_USER:
      return { ...state, error: error.payload };
    case ERRORS.LOGIN_USER:
      return { ...state, error: error.payload };
    case ERRORS.INVITE_EMAIL_TO_PROJECT:
      return { ...state, error: error.payload };
    default:
      return state;
  }
}
