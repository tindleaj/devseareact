import {
  FETCH_ME,
  UNAUTH_USER,
  FETCH_USERS,
  FETCH_USER_PROFILE_PICTURE,
  UPDATE_ME,
  UPDATE_PROFILE_PICTURE,
} from '../actions/types';

export default function(state = {}, action) {
  switch (action.type) {
    case FETCH_ME:
      return {
        ...state,
        me: action.payload.data,
      };

    case FETCH_USER_PROFILE_PICTURE:
      return {
        ...state,
        me: {
          ...state.me,
          profilePicture: action.data,
        },
      };

    case UPDATE_PROFILE_PICTURE:
      debugger;
      return {
        ...state,
        me: {
          ...state.me,
          profilePicture: action.data,
        },
      };

    case UPDATE_ME: {
      return {
        ...state,
        me: action.payload.data,
      };
    }

    case FETCH_USERS:
      return {
        ...state,
        usersList: action.payload.data,
      };

    case UNAUTH_USER:
      return {};

    default:
      return state;
  }
}
